/**************************************************************************
	Handles events for an instance of n-puzzle
	
	Copyright (C) 2020 Babak Razmjoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	To contact with the author mail to : b.razmjoo@protonmail.com
***************************************************************************/

/**
* The main function. Call this once in your HTML page
* @param boardNodeID Id of an object node containing the puzzle
* @size size of the puzzle; it can be 3,4 or 5
*/
function mainNPuzzle(boardNodeID, size)
{
	svg = document.getElementById(boardNodeID).contentDocument.documentElement;
	
	n = size*size - 1; // 3 -> 8 , 4 -> 15 ...
	
	for (i=1;i<=n;i++)
	{
		piece = svg.getElementById("piece"+i);
		if (piece==null)
		{
			console.error("Can't find puzzle piece #"+i);
			return;
		}
		piece.onmousedown = function(event){console.log("Event : "+event)};
		//piece.onmouseup = function(){console.log("Mouse up on piece #"+i)};
	}
}
