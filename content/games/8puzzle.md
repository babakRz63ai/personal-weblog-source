---
title: "معمای پازل ۸"
description: ""
tags: [8-puzzle]
categories: [games]
date: 2020-03-09T13:47:27+03:30
draft: true
custom_js: [npuzzle,load8puzzleGame]
---
<figure>
	<object id="puzzleBoard" type="image/svg+xml" data="../../images/n-puzzles/8puzzle.svg" style="width:35%; min-width:256px;"></object>
</figure>

