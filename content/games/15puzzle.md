---
title: "معمای پازل 15"
description: ""
tags: [15-puzzle]
categories: [games]
date: 2020-03-09T23:22:53+03:30
draft: true
---
<figure>
	<object id="puzzleBoard" type="image/svg+xml" data="../../images/n-puzzles/15puzzle.svg" style="width:35%; min-width:256px;"></object>
</figure>
