---
title: "تبدیل تاریخ میلادی به شمسی"
description: ""
tags: [ابزار,تاریخ شمسی,تقویم جلالی]
categories: []
date: 2019-02-10T14:22:50+03:30
draft: false
---
یک تاریخ میلادی انتخاب کنید یا بنویسید : <input type="date" id="inputDate"/>
<input type="button" onclick="document.getElementById('dateConversionResult').firstChild.nodeValue = convertDateStringToShamsi(document.getElementById('inputDate').value)" value="تبدیل"/>

<b id="dateConversionResult"> </b>
