---
categories: ["projects"]
date: "2016-10-02T22:55:05-04:00"
tags: ["projects"]
title: "پروژه ها"
showpagemeta: false
---
## بانک اطلاعات دایرهٔ مسکن سازمان مسکن و شهرسازی یاسوج
این نرم افزار ویندوزی با استفاده از C#.net و به شکل یک پایگاه داده محلی توسعه داده شد. در این پروژه با آقای حسین صادقیان نژاد همکاری داشتم.

[سازمان مسکن و شهرسازی یاسوج](http://news.mrud.ir/service/%D8%A7%D8%B3%D8%AA%D8%A7%D9%86%E2%80%8C%D9%87%D8%A7/%D9%83%D9%87%D9%83%D9%8A%D9%84%D9%88%D9%8A%D9%87%20%D9%88%20%D8%A8%D9%88%D9%8A%D8%B1%D8%A7%D8%AD%D9%85%D8%AF
)

## اپلیکیشین ارتباط با ردیابهای خودروی شرکت افرا
[![ردیاب افرا](../images/projects/logo-afra-app.png)](http://afra-tech.ir/Page.aspx?id=19)
اپلیکیشن آندرویدی ردیاب خودرو گروه فناوری افرا به کاربر اجازه می دهد آخرین مکان و وضعیت ردیابهای خودروی خود را مشاهده کند.

## اپلیکیشن اندرویدی پزشک اپ
[![Dr.app](../images/projects/logo-drapp.png)](http://dr-yab.ir/)
اپلیکیشن اندرویدی پزشک اپ که سمت کلاینت یک سامانه جامع اطلاعات پزشکان و نوبت دهی آنلاین است. حسین صادقیان  نژاد سمت سرور و وبسایت این پروژه را بر عهده داشتند.

## مدیریت ردیاب خودروی شرکت اورنگ پژوهان پارسه
[![ردیاب اورنگ](../images/projects/logo-orang-tracker.png)](https://play.google.com/store/apps/details?id=orang.tracker)
اپلیکیشن آندرویدی مدیریت ردیاب خودروی شرکت اورنگ پژوهان پارسه درست مانند اپلیکیشن ردیاب خودروی افرا است.

## نرم افزار اندرویدی Orang TAG
[![Orang TAG](../images/projects/logo-orangtag.png)](https://play.google.com/store/apps/details?id=opp.avl.orangtag)
اپلیکیشن آندرویدی ردیاب موبایل و تبلت اورنگ پژوهان پارسه دستگاه آندرویدی را به یک ردیاب تبدیل می کند

## نرم افزار اندرویدی یاور
[![یاور](../images/projects/logo-orang-yaavar.png)](https://www.opp.co.ir/products/avl/%D8%B3%D8%A7%D9%85%D8%A7%D9%86%D9%87-%D8%AA%D9%88%D8%B2%DB%8C%D8%B9-%D9%88-%D9%BE%D8%AE%D8%B4-%D8%A7%D9%88%D8%B1%D9%86%DA%AF/)
اپلیکیشن اندرویدی سامانه دیسپچینگ (Dispatching) شرکت اورنگ پژوهان پارسه که هم اکنون به نام یاور شناخته می شود

