---
title: "۱۰۱ تمرین کدنویسی در اکتاو و متلب"
description: "۱۰۱ تمرین برنامه نویسی فراهم شده اند که می توانند به عنوان ابزار آموزشی اکتاو و یا متلب به کار روند"
tags: [اکتاو,متلب,تمرین,کدنویسی,اسکریپت,Octave,coding,practice]
categories: []
date: 2019-02-22T00:28:17+03:30
draft: true
needsCommenting: true
---
تمرین ابزار آموزشی مهمی است. تمرین چالشی برای آموزنده ایجاد می کند و انگیزه ای به وی می دهد که مطالب آموزشی اش را یادآوری و مرور کند، در قالبهای جدیدی سازمان بدهد، ارتباطی میان آن مطالب و مسالهٔ تمرین ایجاد کند و راه حلی ارائه دهد. همچنین انجام تمرین فرصتی برای کسب تجربه فراهم می‌کند که می تواند در آینده در برخورد با مسائل واقعی عملکرد فرد را بهبود دهد.

اکتاو (Octave) یک بستهٔ نرم افزار آزاد مخصوص محاسبات عددی است. این نرم افزار جزیی از پروژهٔ [GNU](https://www.gnu.org) و معادل متلب برای گنو/لینوکس است. اکتاو با زبان برنامه نویسی متلب کاملاً سازگار است. به علاوه عناصری در برنامه نویسی اکتاو قابل استفاده اند که در متلب تعریف نشده اند. مهندسانی که به کار در متلب عادت دارند می‌توانند اسکریپتهای خود را در اکتاو نیز اجرا کنند، به شرطی که اسکریپتها شامل GUI نباشند یا از جعبه ابزارهایی استفاده نکرده باشند که قابل افزودن به اکتاو نیست.

تمرینهایی که در ادامه می بینید در چند دستهٔ مختلف قرار گرفته اند. هر دسته از تمرینها بر یکی از جنبه های کدنویسی در متلب و اکتاو متمرکزند. در ابتدای هردسته توضیحی آورده ایم که جنبهٔ مورد نظر در همان دسته را نشان می دهند.

## کوتاه ترین کدها
زبان کدنویسی مورد استفاده در اکتاو و متلب بسیار سطح بالا است. در چنین زبانهایی میتوان کارهای پیچیده را با نوشتن یکی دو خط دستور انجام داد. به دلیل معماری خاص متلب و اکتاو و تفسیری بودن زبان کدنویسی، معمولاً کوتاهترین برنامه از نظر اجرا سریعترین برنامه نیز هست. این تمرینها شما را عادت می دهند که همواره از کوتاهترین کد ممکن استفاده کنید.

**۱ تا ۶** فرض کنید متغیر n دارای یک مقدار صحیح مثبت است. برای محاسبهٔ هر یک از موارد زیر تنها یک خط کد بنویسید:

* فاکتوریل n
* مجموع اعداد صحیح مثبت فرد از ۱ تا n
* بزرگترین مقدار <math xmlns="http://www.w3.org/1998/Math/MathML" alt="sine of absolute value of t divided by n minus 5"><semantics><mrow><mi>sin</mi><mrow><mo fence="true" stretchy="true">(</mo><mrow><mrow><mo fence="true" stretchy="true">|</mo><mrow><mrow><mfrac><mrow><mi>t</mi></mrow><mi>n</mi></mfrac><mo stretchy="false">−</mo><mn>5</mn></mrow></mrow><mo fence="true" stretchy="true">|</mo></mrow></mrow><mo fence="true" stretchy="true">)</mo></mrow></mrow><annotation encoding="StarMath 5.0">sin left( abs {t over n - 5} right)</annotation></semantics></math> برای <span class="InlineFormula"><i>t</i>&nbsp;=&nbsp;0,1,…,10<i>n</i></span>
* ماتریس قطری مربعی که اعداد <span class="InlineFormula"><i>i</i>&nbsp;=&nbsp;1,2,…,<i>n</i></span> روی قطر اصلی آن قرار گرفته اند.
* ماتریس مربعی به شکل زیر:
<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><semantics><mrow><mfenced open="[" close="]"><mrow><mtable><mtr><mtd><mrow><mi>n</mi><mo stretchy="false">−</mo><mn>1</mn></mrow></mtd><mtd><mn>0</mn></mtd><mtd><mrow><mi>n</mi><mo stretchy="false">+</mo><mn>1</mn></mrow></mtd></mtr><mtr><mtd><mn>0</mn></mtd><mtd><mi>n</mi></mtd><mtd><mn>0</mn></mtd></mtr><mtr><mtd><mrow><mi>n</mi><mo stretchy="false">+</mo><mn>1</mn></mrow></mtd><mtd><mn>0</mn></mtd><mtd><mrow><mi>n</mi><mo stretchy="false">−</mo><mn>1</mn></mrow></mtd></mtr></mtable></mrow></mfenced></mrow><annotation encoding="StarMath 5.0">left[  matrix{n-1 # 0 # n+1  ## 0 # n # 0 ## n+1 # 0 # n-1}  right]</annotation></semantics></math>

* ماتریس مربعی به شکل زیر:

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><semantics><mrow><mfenced open="[" close="]"><mrow><mtable><mtr><mtd><mn>1</mn></mtd><mtd><mn>2</mn></mtd><mtd><mo stretchy="false">…</mo></mtd><mtd><mi>n</mi></mtd></mtr><mtr><mtd><mn>2</mn></mtd><mtd><mn>3</mn></mtd><mtd><mo stretchy="false">…</mo></mtd><mtd><mrow><mi>n</mi><mo stretchy="false">+</mo><mn>1</mn></mrow></mtd></mtr><mtr><mtd><mo stretchy="false">⋮</mo></mtd><mtd><mo stretchy="false">⋮</mo></mtd><mtd><mo stretchy="false">⋱</mo></mtd><mtd><mo stretchy="false">⋮</mo></mtd></mtr><mtr><mtd><mi>n</mi></mtd><mtd><mrow><mi>n</mi><mo stretchy="false">+</mo><mn>1</mn></mrow></mtd><mtd><mo stretchy="false">…</mo></mtd><mtd><mrow><mn>2</mn><mi>n</mi></mrow></mtd></mtr></mtable></mrow></mfenced></mrow><annotation encoding="StarMath 5.0">left[  matrix{1 # 2 # dotslow # n  ## 2 # 3 # dotslow # n+1 ## dotsvert # dotsvert # dotsdown # dotsvert ## n # n+1 # dotslow # 2n}  right]</annotation></semantics></math>
**۷** با داشتن عدد صحیح مثبت <span class="InlineFormula"><i>n</i></span> یک ماتریس مربعی متقارن با <span class="InlineFormula"><i>n</i></span> سطر و ستون ایجاد کنید که عناصرش از توزیع نرمال استاندارد انتخاب شده باشد.

**۸** با داشتن بردار <span class="InlineFormula"><b>m</b></span> و ماتریس مربعی مثبت و متقارن <span class="InlineFormula"><b>Σ</b></span> قطعه کدی بنویسید که ۱۰۰۰ نمونه از توزیع گاوسی با میانگین <span class="InlineFormula"><b>m</b></span> و ماتریس کواریانس <span class="InlineFormula"><b>Σ</b></span> تولید کند.

## ترسیمات ابتکاری
رسم نمودار بخش مهمی از کار مهندسی است. گاهی لازم است شکلهایی ترسیم کنیم که در مجموعه توابع استاندارد، تابع مخصوصی برایش درنظر نگرفته اند. در این مواقع باید با ترکیبی از ابتکار برنامه نویسی و استفاده از توابع موجود، شکل مورد نظر را رسم کنیم.

**۹** با استفاده از دستور plot قطعه کدهایی برای ترسیم یک دایره، یک بیضی و یک مثلث متساوی الساقین که هر سه ضلعش با هم برابر نیست بنویسید.

**۱۰** 
الف) اسکریپتی بنویسید که با داشتن عدد صحیح مثبت n، n نقطه با مختصات تصادفی در فضای دوبعدی دکارتی تولید و آنها را در نموداری به ترتیب به هم متصل کند. همچنین نقطهٔ آخر را به اولین نقطه متصل کند. آیا همواره یک چندضلعی معتبر به وجود می آید؟  
ب) یکی از روشهای تولید چند ضلعی های معتبر، یعنی طوری که پاره خطها یکدیگر را قطع نکنند استفاده از مختصات قطبی است. اسکریپت قبلی را طوری تغییر دهید که n زاویه و n طول شعاع به شکل تصادفی تولید کند. زوایا باید به ترتیب صعودی تولید شوند. حال این n نقطه را در مختصات قطبی رسم و به هم متصل کنید.  
ج) راهی پیدا کنید که اسکریپت فوق تنها چندضلعی های محدب رسم کند یعنی همهٔ گوشه ها به سوی بیرون شکل باشند.  
د) راهی پیدا کنید که این اسکریپت چندضلعی های منظم رسم کند.

**۱۱** یکی از خاطرات دهه شصتی ها مربوط به یک اسباب بازی آموزشی به نام فیل جادو است. این اسباب بازی از تعدادی چرخ دنده و یک طوقه تشکیل شده بود. طوقه از داخل دنده داشت و چرخ دنده ها درون آن می گشتند. روی هر چرخدنده تعدادی سوراخ تعبیه شده بود. ما طوقه را روی کاغذ می گذاشتیم و یک چرخدنده درونش قرار می دادیم و نوک یک قلم را درون یکی از سوراخهای چرخدنده می گذاشتیم. با چرخاندن چرخدنده به وسیله قلم شکلهای متقارن جالبی رسم می شدند.

اساس کار به این شکل است: یک دایره به شعاع <span class="InlineFormula"><i>r</i><sub>1</sub></span> داریم که روی محیطش مرکز دایره ای به شعاع <span class="InlineFormula"><i>r</i><sub>2</sub></span> قرار دارد. مرکز دایرهٔ دوم با سرعت زاویه ای <span class="InlineFormula"><i>ω</i><sub>1</sub></span> روی محیط دایرهٔ اول حرکت می کند. نقطه ای روی محیط دایرهٔ دوم قرار دارد که با سرعت زاویه ای <span class="InlineFormula"><i>ω</i><sub>2</sub></span> حول مرکز این دایره دوران می کند. بدون استفاده از حلقه های تکرار برنامه ای بنویسید که با گرفتن پارامترهای نامبرده ۵۰۰۰ نمونه از مکان هندسی این نقطه را در صفحه ترسیم کند.

<p style="text-align: center;"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" id="svg8" version="1.1" viewBox="0 0 117.4473 100" height="100mm" width="117.4473mm"><defs id="defs2"><marker style="overflow:visible" id="DotM" refX="0" refY="0" orient="auto"><path transform="matrix(0.4,0,0,0.4,2.96,0.4)" style="fill:#0000fe;fill-opacity:1;fill-rule:evenodd;stroke:#0000fe;stroke-width:1.00000003pt;stroke-opacity:1" d="m -2.5,-1 c 0,2.76 -2.24,5 -5,5 -2.76,0 -5,-2.24 -5,-5 0,-2.76 2.24,-5 5,-5 2.76,0 5,2.24 5,5 z"         id="path4588"/></marker></defs><metadata id="metadata5"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type         rdf:resource="http://purl.org/dc/dcmitype/StillImage"/><dc:title></dc:title></cc:Work></rdf:RDF></metadata>
<g transform="translate(-35.029764,-44.041662)" id="layer1"><ellipse style="opacity:1;fill:none;stroke:#000000;stroke-width:1.16356254;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" ry="49.418221" rx="49.418217" cy="94.041664" cx="85.029762" id="path10"/><ellipse style="fill:none;stroke-width:0.26458332" ry="1.5119048" rx="1.1339285" cy="169.24405" cx="72.949409" id="path4520" /><path id="path4920"  d="m 88.585995,94.269114 46.683295,0.333641" style="fill:none;stroke:#0000fe;stroke-width:1.02039576;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;marker-start:url(#DotM)" />
<text y="105.50454" x="108.191" style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:1.25;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332" xml:space="preserve"><tspan style="font-size:10px;stroke-width:0.26458332" y="102" x="103">r</tspan><tspan style="font-size:6px;stroke-width:0.26458332" y="105" x="107">1</tspan></text>
<text y="105.50454" x="108.191" style="font-style:normal;font-weight:normal;font-size:10.58333302px;line-height:1.25;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332" xml:space="preserve"><tspan style="font-size:10px;stroke-width:0.26458332" y="99" x="145">r</tspan><tspan style="font-size:6px;stroke-width:0.26458332" y="102" x="148">2</tspan></text>
<circle r="17.128531" cy="93.900314" cx="134.97707" id="path5036" style="opacity:1;fill:none;stroke:#0000fe;stroke-width:0.74293751;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"/>
<path id="path5038" d="m 135.26929,94.602754 c 13.74891,-9.87766 13.74891,-9.87766 13.74891,-9.87766" style="fill:none;stroke:#ff0000;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" /> 
</g></svg></p>

## پردازش رشته ها
**۱۲** تابع `unique` عناصر متمایز یک آرایه یا بردار را باز‌می گرداند. اگر یک رشته به آن بدهید کاراکترهای یکتای آن رشته را بازخواهد گرداند. تابع `hist` یک آرایهٔ عددی را گرفته و با داشتن تعدادی بازهٔ منظم، تعداد عناصری از آرایه را می شمارد که در هر بازه قرار دارند و یک هیستوگرام تشکیل می دهد. حال ما می‌خواهیم یک هیستوگرام از عناصر یک رشته داشته باشیم. تابعی بنویسید که با دریافت یک رشته، مشخص کند چه کاراکترهایی در آن وجود دارند و هرکدام چند بار تکرار شده اند.

**۱۳** برنامه ای بنویسید که یک عدد صحیح مثبت را بگیرد و معادل [ابجد](https://fa.wikipedia.org/wiki/%D8%AD%D8%B3%D8%A7%D8%A8_%D8%AC%D9%85%D9%84) آن را تولید کند، یعنی کوتاهترین دنباله از حروف فارسی را تولید کند که به حساب ابجد مساوی عدد مذکور باشد.



## مهندسی شیمی
جان ایتون و همکارانش اولین بار نرم افزار اکتاو را برای حل مسايل مهندسی شیمی توسعه داده بودند. در این قسمت برای حل برخی از مسائل معروف مهندسی شیمی برنامه می نویسید.

**؟؟** فرمول شیمیایی یک ماده به شرطی که پلیمری نباشد نوع و تعداد اتمهای به کار رفته در یکی از مولکولهای آن را نشان می دهد. در صورت پلیمری بودن ماده را با فرمول شیمیایی یکی از مونومرهای آن مشخص می کنند. ما می خواهیم با دریافت فرمول شیمیایی یک ماده یا مونومر، وزن اتمی آن را محاسبه کنیم.
الف: با مراجعه به جداول مرجع شیمی آرایه ای از نماد و جرم اتمی فراوانترین ایزوتوپ عناصر تهیه کنید.  
ب: تابعی بنویسید که فرمول شیمیایی یک مولکول یا مونومر را بگیرد و مشخص کند از هر عنصر چندتا در آن وجود دارد. توجه کنید که گاهی چند اتم یک گروه اتمی تشکیل می دهند، و این گروه خود می تواند چندبار در مولکول یا مونومر ظاهر شود. برای مثال در سولفات آمونیوم با فرمول <span class="InlineFormula">&lrm;(NH<sub>4</sub>)<sub>2</sub>SO<sub>4</sub></span> دو گروه آمونیوم وجود دارد.  
ج: تابعی بنویسید که با دریافت تعداد اتمهای به کار رفته در ملکول یا مونومر، جرم اتمی آن را حساب کند.

**??** 






