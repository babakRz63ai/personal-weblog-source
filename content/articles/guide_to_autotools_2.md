---
title: "راهنمای استفاده از autotools بخش دوم : امکانات نصب"
description: "این مقاله نشان می دهد چگونه با استفاده از autotools اجزای نرم افزار خودمان را روی ماشین مقصد نصی کنیم"
tags: [آموزش,بازمتن,نرم افزار آزاد,autotools,خودکارسازی,توسعهٔ نرم افزار,autoconf,install]
categories: [آموزش]
date: 2020-11-13T15:02:38+03:30
images: ["TODO : Post cover image"]
series: [TODO]
draft: true
needsCommenting: true
containsCode: true
---

روش معمول نصب نرم افزار روی ماشین مقصد با دستور `make install` است. برای این منظور باید حداقل یک هدف به نام `install` به Makefile اضافه شود. این هدف باید شامل دستورهایی باشد که فایلهای لازم را در شاخه های مناسب نصب کند. اگر فایلهای مقصد وجود ندارند باید ساخته شوند. همچنین اگر شاخه های مقصد وجود ندارند نیز باید ایجاد شوند. در ادامه راهنمای قدم به قدم افزودن امکان نصب به بستهٔ نرم افزاری را مشاهده می‌کنید.

## ۱ افزودن ماکروی AC_PROG_INSTALL به configure.ac
این ماکرو وجود یک برنامهٔ نصب کنندهٔ سازگار با BSD را روی ماشین مقصد بررسی می کند. اگر چنین برنامه ای یافت شد مسیرش را در متغیر خروجی `INSTALL` ذخیره می کند. در غیر این صورت مقدار `dir/install-sh -c` را در این متغیر خواهد نوشت به شرطی که اسکریپت مناسبی به نام `install-sh` یا `install.sh` در این مسیر وجود داشته باشد. مسیر `dir` به طور معمول ریشه‌ٔ منبع پروژه است. این ماکرو همچنین متغیرهای خروجی `INSTALL_PROGRAM` و `INSTALL_SCRIPT` را با مقدار `{INSTALL}$` و متغیر خروجی `INSTALL_DATA` را با مقدار زیر تنظیم می کند:

```sh
${INSTALL} -m 644
```

این ماکرو هیچ پارامتری ندارد و به همین شکل `AC_PROG_INSTALL` در configure.ac نوشته می شود.

وقتی از این ماکرو در configure.ac استفاده می کنیم باید اسکریپت install-sh را نیز در کنار این فایل قرار دهیم وگرنه اسکریپت پیکربندی هنگام اجرا پیغام خطای زیر را نشان می دهد و متوقف می شود:

```text
configure: error: cannot find install-sh, install.sh, or shtool in "." "./.." "./../.."
```

ولی اسکریپت install-sh را از کجا می آوریم؟ یک منبع بسیار خوب برای این فایل و سایر فایلهایی که هنگام تولید و بسته بندی نرم افزارهای GNU به آنها نیاز داریم، بستهٔ gnulib-devel است. ابتدا این بسته را با کمک مدیر بسته روی سیستم خود نصب کنید. برای مثال در توزیع فدورا در خط فرمان اجرا کنید:

```sh
$ sudo dnf install gnulib-devel
```
پس از نصب این بسته، از مسیر `usr/share/gnulib/build-aux/` فایل install-sh را در ریشهٔ کد منبع پروژهٔ خود کپی کنید. حال که اینجا هستید اسکریپت mkinstalldirs را نیز از همین مسیر در ریشهٔ پروژهٔ خود کپی کنید و به آن مجوز اجرا بدهید. ما در مراحل بعدی به این فایل نیاز خواهیم داشت.

## ۲ تعریف متغیرها و جانگهدارهای مناسب در Makefile.in

متغیر زیر را در ابتدای Makefile.in تعریف کنید:

```Makefile
INSTALL = @INSTALL@
```

اگر بستهٔ نرم افزاری شما برنامه های اجرایی یا اسکریپتهایی را در ماشین مقصد نصب می کند متغیرهای زیر را نیز تعریف کنید:

```Makefile
INSTALL_PROGRAM = @INSTALL_PROGRAM@
INSTALL_SCRIPT = @INSTALL_SCRIPT@
```

برای نصب هرنوع فایل دیگر، متغیر زیر را تعریف کنید:

```Makefile
INSTALL_DATA = @INSTALL_DATA@
```

## ۳ تعریف اهداف و نوشتن فرمانهای نصب
ما حداقل به هدف install در Makefile نیاز داریم تا اجزای نرم افزار را به شکل پیشفرض روی ماشین مقصد نصب کنیم. پیشنیاز این هدف فایلهایی است که باید نصب شوند.

برای نصب هر برنامهٔ اجرایی فرمانی زیر این هدف می نویسیم که با اجرای `(INSTALL_PROGRAM)$` آن برنامه را در مسیر مناسب نصب کند. برای مثال برای نصب برنامهٔ اجرایی prog1 می نویسیم:

```Makefile
install : prog1
	$(INSTALL_PROGRAM) prog1 $(bindir)/prog1
```

برای نصب همزمان چند برنامه در یک مسیر می توانیم به شکل زیر عمل کنیم:

```Makefile
install : foo bar
	$(INSTALL_PROGRAM) foo bar $(bindir)/
```

برای نصب هر اسکریپت یا هر تعداد اسکریپت در یک مسیر مقصد، از متغیر INSTALL_SCRIPT استفاده می‌کنیم. برای مثال برای نصب دو اسکریپت good.sh و nice.sh در مسیر `bindir` می نویسیم:

```Makefile
install : good.sh nice.sh
	$(INSTALL_SCRIPT) good.sh nice.sh $(bindir)/
```

برای نصب هرنوع فایل دیگر، چه کتابخانه باشد، چه مستندات و چه فایلهای داده، از INSTALL_DATA استفاده می کنیم. برای مثال:

```Makefile
install : libtest.so  test.h  test.png
	$(INSTALL_DATA) libtest.so $(libdir)/mypackage/libtest.so
	$(INSTALL_DATA) test.h $(includedir)/mypackage/test.h
	$(INSTALL_DATA) test.png $(datadir)/mypackage/test.png
```

یکی از مشکلات متداول هنگام نصب فایلها، وجود نداشتن مسیرهای نصب تعریف شده است. ما برای اطمینان از وجود مسیرهای مقصد مناسب از اسکریپت mkinstalldirs استفاده می کنیم. روش اجرای این اسکریپت به شکل زیر است:

```sh
mkinstalldirs [-h] [--help] [--version] [-m MODE] DIR ...
```

برای اینکه make بداند باید مسیرهای مقصد را پیش از نصب فایلها ایجاد کند، ما یک هدف به نام `installdirs` تعریف می‌کنیم و فرمانهای لازم برای ایجاد شاخه های مقصد را در آن می نویسیم. بعد این هدف را به عنوان پیشنیاز هدف `install` ذکر می‌کنیم. برای مثال:

```Makefile
install : libtest.so  test.h  test.png installdirs
	$(INSTALL_DATA) libtest.so $(libdir)/mypackage/libtest.so
	$(INSTALL_DATA) test.h $(includedir)/mypackage/test.h
	$(INSTALL_DATA) test.png $(datadir)/mypackage/test.png

installdirs:
	$(srcdir)/mkinstalldirs $(libdir)/mypackage $(includedir)/mypackage $(datadir)/mypackage
```

چون هردو هدف `install` و `installdirs` غیرفایلی هستند، باید آنها را جلوی `PHONY.` هم بنویسیم:

```Makefile
.PHONY : clean install installdirs
```

