---
title: "راهنمای گام به گام نصب و استفاده از Apache Archiva"
description: "این راهنما برای افراد و سازمانهایی است که با زبان جاوا برنامه نویسی می کنند و برای نگهداری آرشیوهای جاوا یا jar به یک مخزن درون سازمانی نیاز دارند"
tags: [archiva,centOS8,repository,artifacts,java,جاوا]
categories: [آموزش]
date: 2020-03-19T10:31:54+03:30
draft: false
containsCode: true
needsCommenting: true
---
## پیشگفتار
در مقالهٔ [استفاده از Maven برای مدیریت پروژه های جاوا](../using-maven-and-archiva/) نرم افزارهای Maven و Archiva را معرفی کردیم که هردو از محصولات آپاچی هستند و برای مدیریت پروژه های جاوا به کار می‌روند. در این مقاله ابتدا به روش گام به گام روش نصب Archiva روی یک سرور CentOS8 را آموزش می دهیم، بعد نشان می دهیم چطور می توانید مسقیما برای Maven یا در محیطهای توسعهٔ IntelliJ و Android Studio از این مخزن اختصاصی استفاده کنید.

## نصب و برپاکردن سرور
در این آموزش قصد داریم از CentOS 8 Stream استفاده کنیم. اگر هنوز سرور خودتان را راه اندازی نکرده اید، به [این صفحه از سایت centos.org](https://wiki.centos.org/Download/) بروید و از یکی از mirror های موجود یک فایل ISO برای CentOS 8 Stream دانلود کنید. ما توصیه می کنیم ISO نوع net install را انتخاب کنید تا از دانلود حجم زیاد و بیهوده‌ٔ DVD ISO اجتناب کنید. صحت این فایل را طبق [این راهنما](https://wiki.centos.org/TipsAndTricks/sha256sum) تایید کنید و بعد روی سرور خودتان نصب کنید. ما برخی از مراحل این آموزش را به شکل گرافیکی در cockpit نشان می دهیم. پس در زمان نصب در Anaconda طبق تصاویر زیر حداقل پیکربندی لازم را برای برپایی یک سرور به همراه پنل گرافیکی cockpit تعیین کنید.

[![Enable networking and open softwares](../../images/centos8/anaconda-config-sz.jpg "Enable networking and open softwares")](../../images/centos8/anaconda-config.png)

[![Set as server and select graphical administration tools](../../images/centos8/anaconda-softwares-sz.jpg "Set as server and select graphical administration tools")](../../images/centos8/anaconda-softwares.png)


## نصب Open JDK 8
نرم افزار Archiva یک اپلیکیشین جاوا است. بنابراین شما روی سرور خود به ماشین مجازی جاوا نیاز دارید. به این منظور باید Open JDK 8 را نصب کنید. Archiva از جاوای ۱۱ پشتیبانی نمی کند بنابر این نصب Open JDK 11 یا نسخه های بالاتر بی فایده است. در خط فرمان سرور بنویسید:

```shell
$ yum search openJDK
```

بعد نسخهٔ Open JDK 8 مناسب سرور خود را بیابید و با `yum install` نصب کنید.

## دانلود بستهٔ نصبی Archiva
به [صفحه دانلود Archiva](http://archiva.apache.org/download.cgi) بروید و آخرین نسخهٔ پایدار این نرم افزار را دانلود کنید که یک فایل زیپ شده است. آخرین نسخهٔ Archiva در زمان نوشتن این مقاله 2.2.4 بود. توجه کنید که Archiva نسخه های نصبی متفاوتی دارد. ما روش نصب نسخهٔ **Standalone** را آموزش می دهیم پس شما هم همین نسخه را دانلود کنید. همچنین فرمت tar.gz را انتخاب کنید که حجم کمتری دارد. طبق راهنمایی که در همان صفحه نوشته اند صحت آرشیو دانلود شده را ارزیابی کنید.

## نصب Archiva
نرم افزار Archiva پس از نصب، مخزن مصنوعات جاوا را که برحسب نیاز دانلود می کند یا شما به آن می افزایید در جایی روی دیسک می سازد فایلها را در آن ذخیره می کند. همچنین این نرم افزار در زمان اجرا وقایع خود را در چند log file ثبت می کند. ما در این آموزش قصد داریم Archiva را طوری نصب و پیکربندی کنیم که این فایلهای متغیر را که حاصل فعالیت کاربران است جایی جدا از محل نصب خود Archiva ذخیره کند. به ویژه ما می خواهیم Archiva را در یکی از زیرشاخه های `usr/libexec/` نصب کنیم ولی آن را وادار کنیم مانند Apache webserve فایلهای کاربر را در یکی از زیرشاخه های `var/` ذخیره کند.

می دانید که هر برنامه یا سرویس در گنو/لینوکس با یک کاربر معین اجرا می شود. برنامه هایی که شما اجرایشان را در خط فرمان آغاز می کنید تحت حساب کاربری خودتان اجرا می شوند. بیشتر سرویسهایی که با روشن شدن سیستم به شکل خودکار شروع می شوند تحت کاربر مدیر یا `root` اجرا می شوند. امکان دارد برخی از این سرویسها را با کاربر دیگری غیر از `root` هم اجرا کنیم. امکان دارد Archiva رانیز طوری پیکربندی کنیم که ماشین مجازی جاوا را تحت کاربری غیر از root اجرا کند. 

به این دو دلیل ممکن است مراحل این بخش از آموزش پیچیده و طولانی به نظر برسند. پس با صبر و حوصله همراه ما باشید.

### کپی کردن فایلها در `usr/libexec/`
فایل نصب Archiva را که در فرمت tar.gz است با `cp` یا `scp` در `usr/libexec/` کپی کنید. سپس با دستور زیر فایلهایش را استخراج کنید:

```shell
$ cd /usr/libexec
$ sudo tar -xzf apache-archiva-2.2.4-bin.tar.gz
$ ls apache-archiva-2.2.4
apps  bin  conf  contexts  lib  LICENSE  logs  NOTICE  temp
```

اکنون باید پوشه ای به نام apache-archiva-2.2.4 در مسیر `usr/libexec/` ساخته شده باشد. Archiva تعدادی اسکریپت به نام wrapper در زیرشاخهٔ bin دارد که امکان می دهند این نرم افزار به شکل standalone اجرا شود. فایلهای ثبت وقایع به شکل پیشفرض در زیرشاخهٔ logs ذخیره می شوند. فایلهای پیکربندی نیز در زیرشاخهٔ conf قرار دارند.

ابتدا محتویات زیرشاخهٔ bin را نگاه کنید:

```shell
$ ls apache-archiva-2.2.4/bin
```

```text
archiva               wrapper-macosx-universal-32  wrapper-solaris-x86-32
archiva.bat           wrapper-macosx-universal-64  wrapper-windows-x86-32.exe
wrapper-linux-x86-32  wrapper-solaris-sparc-32     wrapper-windows-x86-64.exe
wrapper-linux-x86-64  wrapper-solaris-sparc-64
```

برخی از این اسکریپتها مخصوص ویندوز است، بعضی مخصوص macOS است و غیره. می توانید اسکریپتهایی را که لازم ندارید حذف کنید:

```shell
$ sudo rm apache-archiva-2.2.4/bin/*.bat apache-archiva-2.2.4/bin/*windows* apache-archiva-2.2.4/bin/*macosx* apache-archiva-2.2.4/bin/*solaris*
```

حال وقت آن است که Archiva را در حالت local آزمایش کنید. در خط فرمان سرور بنویسید:

```shell
$ sudo /usr/libexec/apache-archiva-2.2.4/bin/archiva console
```

```text
Running Apache Archiva...
wrapper  | --> Wrapper Started as Console
wrapper  | Launching a JVM...
jvm 1    | OpenJDK 64-Bit Server VM warning: ignoring option MaxPermSize=128m; support was removed in 8.0
jvm 1    | Wrapper (Version 3.2.3) http://wrapper.tanukisoftware.org
jvm 1    | 
jvm 1    | 2020-03-21 22:54:00.169:WARN:oejd.ContextDeployer:ContextDeployer is deprecated. Use ContextProvider
.
.
.
jvm 1    | 2020-03-21 22:55:20.452:INFO:oejs.AbstractConnector:Started SelectChannelConnector@0.0.0.0:8080
```

وقتی آخرین خط پیام به شکل بالا چاپ شد یعنی وب سرویس Archiva به درستی اجرا شده و در آدرس localhost:8080 در دسترس است. یک ترمینال دیگر روی سرور باز کنید و در آن اجرا کنید:

```shell
$ curl "http://localhost:8080"
```

باید یک صفحه وب به شکل زیر را مشاهده کنید:

```html
<!DOCTYPE html>

<!--
~  ~ Licensed to the Apache Software Foundation (ASF) under one
~  ~ or more contributor license agreements.  See the NOTICE file
~  ~ distributed with this work for additional information
~  ~ regarding copyright ownership.  The ASF licenses this file
~  ~ to you under the Apache License, Version 2.0 (the
~  ~ "License"); you may not use this file except in compliance
~  ~ with the License.  You may obtain a copy of the License at
~  ~
~  ~  http://www.apache.org/licenses/LICENSE-2.0
~  ~
~  ~ Unless required by applicable law or agreed to in writing,
~  ~ software distributed under the License is distributed on an
~  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~  ~ KIND, either express or implied.  See the License for the
~  ~ specific language governing permissions and limitations
~  ~ under the License.
~  -->

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css"/>
    <link rel="stylesheet" href="css/jqueryFileTree.css"/>
    <link rel="stylesheet" href="css/jquery-ui-1.9.2.custom.min.css"/>
    <link rel="stylesheet" href="css/select2-3.2.css"/>
    <link rel="stylesheet" href="css/typeahead.js-bootstrap.0.9.3.css"/>
    <link rel="stylesheet" href="css/bootstrap.2.2.2.css">
    <link rel="stylesheet" href="css/archiva.css">
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" href="css/prettify.css"/>

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="js/sammy.0.7.4.js"></script>
    <script type="text/javascript" data-main="js/archiva/archiva.js" src="js/require.min.2.1.11.js"></script>

    <title>Apache Archiva</title>

  </head>

  <body id="body_content" style="padding-top: 42px;">

    <div id="topbar-menu-container"></div>

    <div class="container-fluid" style="min-height: 550px">
      <div class="row-fluid">
        <div class="span2 columns">
          <div class="well sidebar-nav" id="sidebar-content"></div>
        </div>
        <div class="span10 columns">
          <div class="content">
            <div id="user-messages"></div>
            <div id="main-content"></div>
          </div>
        </div>
      </div>

    </div>

    <footer id="footer-content" style="vertical-align: bottom">
    </footer>

    <div id="html-fragments"></div>

    <div id="loadingDiv">
      <div class="loading-indicator">
        <img src="images/medium-spinner.gif" alt="Archiva loading"/>
        <span id="loading-msg">Loading</span>
      </div>
    </div>
    <noscript>
    Archiva needs Javascript, please check the Javascript configuration of your browser
    </noscript>
  </body>

</html>
```

در نهایت با ترکیب `ctrl+C` نمونهٔ در حال اجرای Archiva را ببندید.

### نصب Archiva به شکل سرویس
یک لینک در etc/init.d/ به اسکریپت اجرایی archiva ایجاد کنید:

```shell
$ sudo ln -s /usr/libexec/apache-archiva-2.2.4/bin/archiva /etc/init.d/archiva
```

برای اطمینان از درستی کار، می توانید با دو دستور زیر Archiva را به شکل سرویس روشن و خاموش کنید:

```shell
$ sudo service archiva start
⁞
$ sudo service archiva stop
```

بعد Archiva را به شکل یک سرویس به طور دایمی ثبت کنید تا هروقت سرور روشن شد به طور خودکار اجرا شود:

```shell
$ sudo systemctl enable archiva
```

همچنین لازم است فایروال سرور اجازه‌ٔ دسترسی به پورت ۸۰۸۰ را بدهد. به این منظور مانند تصاویر زیر یک سرویس به قواعد فایروال اضافه کنید:

[![Open firewall settings on cockpit panel](../../images/centos8/open-firewall-sz.jpg "Click on Networking, find Firewall and open 'Add Services'")](../../images/centos8/open-firewall.png)

[![Open port 8080 for Archiva](../../images/centos8/firewall-for-archiva-sz.jpg "Add a custom HTTP service on port 8080")](../../images/centos8/firewall-for-archiva.png)

حالا سرویس Archiva را با دستوری زیر راه اندازی کنید.
```shell
$ sudo service archiva start
```


ممکن است راه اندازی کامل سرویس حدود یک یا دو دقیقه طول بکشد. برای اطمینان از عملکرد Archiva تغییرات فایل log را دنبال کنید:

```shell
$ tail -f /usr/libexec/apache-archiva-2.2.4/logs/wrapper-yyyymmdd.log
```

بعد از راه افتادن کامل سرویس Archiva، در یک مرورگر از یک رایانه غیر از سرور جدید، پورت ۸۰۸۰ سرور را باز کنید. باید صفحهٔ ابتدایی Archiva را به شکل زیر ببینید:

[![Starting page of Apache Archiva](../../images/archiva/start-page-sz.jpg)](../../images/archiva/start-page.png)

---

در این مرحله نمونهٔ Archiva آمادهٔ کاربرد به شکل یک وب سرویس و مخزن اختصاصی است. اما همچنان تحت حساب کاربری root اجرا می شود و طی گذر زمان حجم زیادی از آرشیوهای جاوا و فایلهای log را در usr/libexec/ ذخیره خواهد کرد. پس با ما همراه باشید تا آخرین مرحلهٔ پیکربندی Archiva را انجام دهیم.

### ساخت یک کاربر سیستمی برای Archiva و جداسازی فایلهای متغیر data از فایلهای ثابت

سرویس Archiva را متوقف کنید:
```shell
$ sudo service archiva stop
```

یک حساب کاربری سیستمی و یک گروه به نام archivau بسازید. این کاربر باید بتواند فرمانهایی را در پوسته اجرا کند:

```shell
$ sudo useradd -rUs /usr/bin/sh archivau
```
پوشه های زیر را در مسیر var/ بسازید:

```shell
$ sudo mkdir -p /var/archiva/conf
$ sudo mkdir /var/archiva/data
$ sudo mkdir /var/archiva/logs
$ sudo mkdir /var/archiva/temp
$ sudo mkdir -p /var/archiva/repositories/internal
$ sudo mkdir -p /var/archiva/repositories/snapshots
```

تمام فایلهای پیکربندی Archiva را در مکان جدید کپی کنید (یک نسخه از آنها باید در مسیر قبلی باقی بماند پس با mv انتقال ندهید):

```shell
$ sudo cp /usr/libexec/apache-archiva-2.2.4/conf/* /var/archiva/conf/
```

مالک و گروه همه‌ٔ این فایلها و پوشه های جدید را archivau قرار دهید:

```shell
$ sudo chown -R archivau:archivau /var/archiva
```

متغیر محیطی `ARCHIVA_BASE` را تعریف کنید تا به آدرس `var/archiva/` اشاره کند. به این منظور دستور زیر را اجرا کنید تا یک خط به انتهای etc/environment/ اضافه شود:

```shell
$ echo "ARCHIVA_BASE=/var/archiva" | sudo tee -a /etc/environment
```

حال اسکریپت اجرایی archiva را باز کنید:

```shell
$ sudo vi /usr/libexec/apache-archiva-2.2.4/bin/archiva
```

این خط را در آن پیدا کنید (احتمالا خط شمارهٔ ۸۵ است):

```shell
#RUN_AS_USER=
```

و به شکل زیر تغییر دهید، تغییرات را ذخیره کنید و خارج شوید:

```shell
RUN_AS_USER=archivau
```

اکنون می توانید فایلهایی ثبت وقایعی را که Archiva تاکنون در usr/libexec/ ذخیره کرده است پاک کنید:

```shell
$ sudo rm -rf /usr/libexec/apache-archiva-2.2.4/logs/*
$ sudo rm -rf /usr/libexec/apache-archiva-2.2.4/repositories
```

با وجود تمام این پیکربندی ها، Archiva هنوز سعی می کند در زمان شروع به اجرا، یک فایل کوچک به نام archiva.pid را در زیرشاخهٔ logs مسیر نصب خود بسازد، و در زمان اتمام اجرا آن را حذف کند. من نمی دانم این رفتار از قبل پیشبینی شده یا یک ایراد در نرم افزار Archiva است. [پیوند گزارش اشکال Archiva](http://archiva.apache.org/issue-tracking.html) در سایت آپاچی نیز ما را به صفحهٔ **404** هدایت می کند. بنابراین از یک راه حل موقت برای این مساله استفاده می کنیم: به کاربر archivau مجوز نوشتن روی زیرشاخهٔ logs قبلی را می دهیم. به این منظور از `setfacl` استفاده می کنیم:

```shell
$ cd /usr/libexec/apache-archiva-2.2.4
$ sudo setfacl -m u:archivau:rwx logs
```

خوب، حالا می توانید سرویسها را از نو روی سرور راه اندازی کنید، تا Archiva با پیکربندی جدید شروع به کار کند:

```shell
$ sudo systemctl daemon-reload
$ sudo service archiva start
```
حال دوباره در یک مرورگر به پورت 8080 سرور خود مراجعه کنید. باید صفحه اول Archiva را بعد از یک یا دو دقیقه ببینید. 

### پیکربندی Archiva
#### ثبت کاربر admin برای Archiva
در همین صفحهٔ اول روی دکمهٔ **Create Admin User** بزنید تا دیالوگ ساخت کاربر Admin باز شود. فیلدهای لازم را طبق شکل زیر پر کنید و ذخیره کنید.

[![Create the Admin user in Archiva](../../images/archiva/create-admin-sz.jpg)](../../images/archiva/create-admin.png)

اگر همه چیز درست پیش برود، اکنون باید بتوانید گزینه های مدیریتی Archiva را ببینید.

#### تعیین محل نگهداری مخازن
گزینهٔ *Repositories* را از منوی سمت چپ انتخاب کنید تا صفحهٔ مدیریت مخازن مانند شکل زیر باز شود.

[![Manage repositories](../../images/archiva/edit-managed-repositories-sz.jpg)](../../images/archiva/edit-managed-repositories.png)

دکمه های *Edit* جلوی هرکدام از ردیفهای *internal* و *snapshots* را بفشارید و مسیر ذخیرهٔ فایلهای هر کدام را به ترتیب مانند دو شکل زیر تغییر دهید. **توجه:** به دلیل اشکالی که در نسخهٔ ۲.۲.۴ وجود دارد از آدرسهای مطلق استفاده کنید.

[![Set internal repository](../../images/archiva/set-internal-repository-sz.jpg)](../../images/archiva/set-internal-repository.png)

[![Set snapshots repository](../../images/archiva/set-snapshots-repository-sz.jpg)](../../images/archiva/set-snapshots-repository.png)

#### دادن مجوز مشاهدهٔ مخازن و دانلود مصنوعات به کاربران مهمان (اختیاری)
با این پیکربندی دیگر لازم نیست برای مشاهده‌ٔ مصنوعاتی که در مخازن Archiva ذخیره شده اند با نام کاربری و رمز عبور وارد شوید. هر کسی به سرور دسترسی داشته باشد می تواند از مخازن این نمونهٔ Archiva استفاده کند. برای این کار از منوی سمت چپ زیر فهرست **USERS** گزینهٔ **Manage** را انتخاب کنید. فهرست حسابهای کاربران باز می شود. در ردیف guest دکمهٔ ویرایش (Edit) را بزنید.

[![Open users list and edit guest](../../images/archiva/open-user-guest-for-editing-sz.jpg)](../../images/archiva/open-user-guest-for-editing.png)

صفحهٔ ویرایش مشخصات کاربر مهمان مثل شکل زیر باز می شود. دکمهٔ **Edit Roles** را بفشارید.

[![Press Edit Roles for guest users](../../images/archiva/start-editing-roles-guest-sz.jpg)](../../images/archiva/start-editing-roles-guest.png)

در صفحهٔ ویرایش نقشهای کاربر مهمان مانند شکل زیر، گزینه های **Repository Observer** را برای ردیفهای *snapshots* و *internal* فعال کنید و با فشردن دکمهٔ *update* تغییرات را ذخیره کنید.

[![Edit roles for guest](../../images/archiva/edit-roles-guest-sz.jpg)](../../images/archiva/edit-roles-guest.png)
---

## استفاده از نمونه‌ٔ Archiva به عنوان مخزن در اندروید استودیو

می توانید اندروید استودیو را تنظیم کنید تا کتابخانه های جاوا را که خودتان توسعه می دهید از مخزن اختصاصی خودتان دریافت کند. به این منظور فایل `build.gradle` پروژهٔ اندروید خودتان را باز کنید و آدرس مخزن خودتان را به `repositories` در `allprojects` اضافه کنید:

```gradle
allprojects {
    repositories {
    	mavenCentral()
        google()
        maven {url "http://192.168.xxx.yyy:8080/repository/internal/"}
      }
}
```
توجه داشته باشید که هر یک از ما‌ژولهای پروژهٔ اندرویدی از جمله اپلیکیشین ها نیز هر کدام فایل `build.gradle` خود را دارند. شما باید این فایل را در مسیر اصلی پروژه تغییر دهید.

اگر بخواهید gradle برای دانلود کتابخانه های جاوا تنها از مخزن اختصاصی شما استفاده کند نام سایر مخازن مثل `()mavenCentral` و `()google` را حذف کنید.

می توانید از این هم جلوتر بروید و اندروید استودیو را وادار کنید برای دانلود کتابخانه های مورد نیاز خودش نیز (مثل gradle یا play services) از مخزن اختصاصی شما استفاده کند. به این منظور تغییرات پیشنهادی را در همین فایل build.gradle در بخش `buildscript` نیز انجام دهید. در مجموع فایل build.gradle به شکل زیر در می آید:

```gradle
buildscript {
    repositories {
        maven {url "http://192.168.xxx.yyy:8080/repository/internal/"}
    }
    
    dependencies {
        classpath 'com.android.tools.build:gradle:x.y.z'
        
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        maven {url "http://192.168.xxx.yyy:8080/repository/internal/"}
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```

## استفاده از Archiva به عنوان مخزن در IntelliJ IDEA و Maven
این محیط توسعه می تواند ابزارهای (build tools) مختلفی از جمله maven را استفاده کند. درصورت استفاده از maven می توانید این محیط توسعه را تنظیم کنید تا کتابخانه های جاوا را از مخزن اختصاصی دریافت کند. همچنین این امکان دارد که در مرحلهٔ انتشار (Deploy)، کتابخانه یا اپلیکیشن جاوای خودتان را روی مخزن اختصاصی قرار دهید.

### استفاده از مخزن اختصاصی برای دریافت وابستگی ها در یک پروژه
در فایل POM پروژه درون عنصر `<project>` باید عنصری به نام `<repositories>` باشد. میتوانید به ازای هر مخزن اختصاصی دو فرزند با نام `<repository>` با ساختارهایی به شکل زیر به `<repositories>` بیافزایید. یکی از این دو عنصر `<repository>` برای دریافت مصنوعات در حالت *release* و دیگری برای حالت *snapshot* است.

```xml
  <repositories>
    <repository>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <id>corporate-release</id>
      <layout>default</layout>
      <url>http://192.168.xxx.yyy:8080/repository/internal/</url>
    </repository>

    <repository>
      <releases>
        <enabled>false</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
      <id>corporate-snapshots</id>
      <layout>default</layout>
      <url>http://192.168.xxx.yyy:8080/repository/snapshots/</url>
    </repository>
  </repositories>
```

**توجه** در XML بالا دو عنصر `<id>` درون هر ‍‍`<repository>` می بینید. من هنوز مطمئن نیستم وجود این عناصر لازم است یا نیست، و دقیقا کاربردشان چیست. امیدوارم در آینده برای بررسی دقیقتر این موضوع فرصتی به دست آورم.

امکان تعیین تنظیمات بیشتری برای هر مخزن موجود است. برای آگاهی از همهٔ تنظیمات ممکن [راهنمای Maven مرجع POM بخش مخازن](http://maven.apache.org/pom.html#Repositories) را ببینید.

### انتشار مصنوعات جاوا در مخزن Maven اختصاصی
برای این کار لازم است یک حساب کاربری با مجوز مدیریت مخزن در Archiva داشته باشید، تنظیمات Maven را خارج از IntelliJ IDEA تغییر دهید و بعد در فایل POM پروژه های جاوا که می خواهید روی مخزن خودتان منتشر کنید نیز تغییراتی بدهید.

#### ساخت حساب کاربری توسعه دهنده در Archiva
اگرچه با حساب کاربری Admin می توانید در Archiva همه کار انجام دهید، ولی بهتر است برای کارهای روتین توسعه و برنامه نویسی حساب جداگانه داشته باشید. پس کسی که حساب کاربری admin را دراختیار دارد باید یک حساب کاربری جدید بسازد. به این منظور پس از ورود به حساب admin مانند شکل زیر از منوی سمت چپ زیر فهرست **USERS** گزینهٔ **Manage** را انتخاب کنید. فهرست حسابهای کاربران باز می شود.

[![Add a new user in Archiva](../../images/archiva/add-new-user-sz.jpg)](../../images/archiva/add-new-user.png)

صفحهٔ افزودن کاربر را با فشردن سرایند **Add** باز کنید. مانند شکل زیر مشخصات یک کاربر جدید را وارد کنید و با دکمهٔ save ذخیره کنید.

[![Add a new user named developer1](../../images/archiva/add-user-developer1-sz.jpg)](../../images/archiva/add-user-developer1.png)

حال به فهرست کاربران باز می‌گردید درحالی که کاربر جدید به انتهای فهرست افزوده شده است.

[![User developer1 is added. Now press Edit button on its row](../../images/archiva/developer1-added-edit-sz.jpg)](../../images/archiva/developer1-added-edit.png)

مانند شکل بالا دکمهٔ Edit را در ردیف این حساب کاربری جدید بفشارید. صفحهٔ ویرایش حساب کاربری developer1 مانند شکل زیر باز می شود.

[![Edit user account for developer1](../../images/archiva/edit-account-developer1-sz.jpg)](../../images/archiva/edit-account-developer1.png)

حال دکمهٔ **Edit Roles** را بفشارید. در صفحهٔ ویرایش نقشهای کاربر مانند شکل زیر، دو نقش Repository Manager و Repository Observer را برای دو نوع مخزن internal و snapshots فعال کنید و با فشردن دکمهٔ Update ذخیره کنید.

[![Edit roles for developer1](../../images/archiva/edit-roles-developer1-sz.jpg)](../../images/archiva/edit-roles-developer1.png)

#### تغییر تنظیمات Maven
هر توسعه دهنده در مسیر m2. از پوشهٔ خانگی خودش به یک فایل تنظیمات Maven با نام settings.xml نیاز دارد. اگر این فایل قبلا ساخته شده آن را باز کنید وگرنه یک فایل جدید بسازید. اگر عنصر `servers` وجود ندارد آن را اضافه کنید. بعد یک عنصر `server` با محتویاتی مثل نمونهٔ زیر به عنصر `servers` بیافزایید. در مجموع فایل settings.xml باید مثل فهرست زیر باشد.

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                          https://maven.apache.org/xsd/settings-1.0.0.xsd">
	<servers>
		<server>
			<id>corporate-repo</id>
			<username>developer1</username>
			<password>my_password</password>
		</server>
	</servers>                   
</settings>
```

#### تغییر فایل POM پروژه
در انتهای فایل POM هر پروژه ای که باید در مخزن اختصاصی منتشر شود درون عنصر `project`،  عنصر `distributionManagement` را با محتویات زیر اضافه کنید:

```xml
	<distributionManagement>
		<repository>
			<uniqueVersion>false</uniqueVersion>
			<id>corporate-repo</id>
			<name>Corporate Archiva Repository</name>
			<url>http://192.168.xxx.yyy:8080/repository/internal/</url>
			<layout>default</layout>
		</repository>
	</distributionManagement>
```

تمام! از این به بعد هروقت مرحلهٔ چرخهٔ حیات deploy را بر پروژه اجرا کنید، یک نسخه از پروژه در قالب jar به همراه یک فایل POM با شمارهٔ نسخهٔ جاری در مخزن Archiva قرار می گیرد.

