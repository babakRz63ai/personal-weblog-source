---
title: "راهنمای نرم افزار autotools : autoconf, autoscan and autoheader"
description: "بستهٔ نرم افزاری autotools مجموعه ای از ابزارها است که برای خودکارسازی فرایندهای پیکربندی، کامپایل، نصب و بسته بندی نرم افزار در گنو/لینوکس به کار می رود"
tags: [آموزش,بازمتن,نرم افزار آزاد,autotools,خودکارسازی,توسعهٔ نرم افزار,autoconf,autoscan,autoheader]
categories: [آموزش]
date: 2019-12-13T17:27:36+04:30
containsCode: true
needsCommenting: true
draft: false
---
## مقدمه
نوشتن کتاب، مدیریت و نگهداری یک وبسایت یا توسعهٔ نرم افزار فرایندهای خلاقانه ای هستند. با این وجود نویسنده یا برنامه نویس  لازم است روزانه اعمال تکراری زیادی را انجام دهد. برخی از نویسندگان یا توسعه دهندگان برای خودکارسازی این کارهای تکراری از ابزارهایی مثل [GNU make](../guide_to_make/) استفاده می کنند. اما هنگام توسعه‌ٔ پروژه های بزرگ، حتی کار با GNU make و نوشتن Makefile نیز می تواند تکراری و خسته کننده باشد. اینجا است که باید به دنبال راه حلی باشیم که کارهای تکراری لازم برای نوشتن Makefile را نیز خودکارسازی کنیم. بستهٔ نرم افزاری Autotools برای همین هدف ساخته شده است. ابزارهای این بسته به طور کلی اعمال لازم برای پیکربندی، ساخت، نصب و بسته بندی پروژه های نرم افزار آزاد روی ماشینهایی با معماری ها و تنظیمات مختلف را به شکل خودکار انجام می دهند. در ادامه مثالی از پروژهٔ نرم افزاری می آوریم و قدم به قدم با استفاده از Autotools برایش سازوکار پیکربندی، کامپایل، نصب و بسته بندی را فراهم می کنیم.

## مثال : پروژهٔ Arty
من و همسن و سالهایم زمانی به دبیرستان رفتیم که هنوز ویندوز ۹۸ ساخته نشده بود. کامپیوترهایی که در دسترس ما بودند با سیستم عامل DOS کار می کردند که تنها یک خط فرمان داشت؛ ما هیچ نمی دانستیم سیستم عاملی به نام GNU/Linux هم وجود دارد. روشن است که آن زمان محیطهای برنامه نویسی جدید مثل ویژوال استودیو توسعه نیافته بود. ما برنامه نویسی هایمان را در Turbo pascal ،Qbasic یا Turbo C انجام می دادیم. Turbo pascal و Turbo C هردو متلعق به شرکت Borland بودند؛ همان شرکتی که بعدها دلفی را توسعه داد. در بسته های نرم افزاری Turbo pascal و Turbo C کد منبع تعدادی برنامهٔ نمونه هم قرار داشت. یکی از این برنامه ها Arty نام داشت. این برنامه یک جور screen saver بود، یعنی بدون وقفه شکلهای گرافیکی خاصی را با سرعت کنترل شده رسم می کرد. در این قسمت از مقاله می خواهیم این برنامه را از نو با ++C بنویسیم. ولی این بار به جای DOS در GNU/Linux کار می‌کنیم، و به جای کتابخانهٔ گرافیکی Borland از SDL استفاده می کنیم. نسخهٔ آن زمان Arty اشکال گرافیکی را با الگوی ثابتی رسم می کرد. برای بیشتر شدن چالش، می خواهیم در برنامهٔ خودمان کاری کنیم که شکلهای گرافیکی علاوه بر الگوهای ثابت قدیمی، حرکت موشواره را نیز روی صفحه دنبال کنند.

ما کد منبع این پروژه را در [مخزنی روی فراماگیت]() قرار داده ایم. این مخزن دارای یک شاخه (branch) اصلی است و تمام مراحل این آموزش در کامیت (commit) های مختلف همین شاخه قرار دارند. برای اینکه بتوانید این آموزش را روی کد منبع دنبال کنید لازم است کل مخزن را با عمل clone دریافت کنید. کد منبعی که با عمل دانلود دریافت می شود فاقد تاریخچهٔ تغییرات فایلها است. این تاریخچه مراحل مختلف تغییراتی را نشان می دهد که با هر مرحله از این آموزش روی کد منبع اعمال شده است. **توضیح برچسبها**

بیایید نگاهی به سازماندهی کد منبع اولیه بیاندازیم. من این پروژه را با پیروی از معماری [MVP]() کد نویسی کردم. این معماری دلیل تعدد فایلهای سرایند و پیاده سازی های ++C است. هر سرایند یک کلاس را اعلان می کند، و فایل ++C همنام با این سرایند آن کلاس را پیاده سازی می کند. البته به جز main.cxx و contract.h. فایل main.cxx همانطور که حدس می زنید شامل کد تابع main است. سرایند contract.h واسطهایی (interface) را اعلان می کند که کلاسهای اصلی معماری MVP باید پیاده سازی کنند. در ادامه سلسله مراتب شاخه ها و فایلهای اصلی پروژه را می بینید.

```
   src
    ├── contract.h
    ├── eventloop.cxx
    ├── eventloop.h
    ├── graphics
    │   ├── sdlview.cxx
    │   └── sdlview.h
    ├── main.cxx
    ├── model
    │   ├── line.h
    │   ├── stroke.cxx
    │   └── stroke.h
    ├── presenter.cxx
    └── presenter.h
```

### مرحلهٔ اول
کد مرحلهٔ اول این آموزش تحت برچسب Stage1 قرار دارد. برای رفتن به این مرحله در مخزن کد پروژه، در ریشهٔ پروژه وارد کنید:

```sh
git checkout Stage1
```

کامپایل کردن این پروژه تنها با استفادهٔ مستقیم از خط فرمان کار بسیار سختی است. به این دلیل در اولین مرحله یک Makefile به پروژه می افزاییم تا کامپایل و پیوندزدن فایل اجرایی را به شکل خودکار انجام دهد. در ادامه این فایل را می بینید. اگر محتوای این فایل برایتان نامفهوم است یا در نوشتن Makefile به راهنمایی نیاز دارید به [مقالهٔ آموزش نوشتن Makefile](../guide_to_make/) نگاهی بیاندازید.

```makefile
objects = stroke.o presenter.o sdlview.o main.o eventloop.o  

LDFLAGS = -lSDL2
CPPFLAGS = -I/usr/include/SDL2

arty : $(objects)
	g++ -o arty $(objects) $(LDFLAGS)

eventloop.o : src/eventloop.cxx  src/eventloop.h  src/contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

sdlview.o : src/graphics/sdlview.cxx  src/graphics/sdlview.h  src/contract.h
	g++ $(CPPFLAGS) -Isrc -o $@ -c $<

main.o : src/main.cxx  src/presenter.h  src/graphics/sdlview.h  src/eventloop.h
	g++ $(CPPFLAGS) -Isrc -Isrc/model -Isrc/graphics -o $@ -c $<

stroke.o : src/model/stroke.cxx  src/model/stroke.h  src/model/line.h
	g++ $(CPPFLAGS) -o $@ -c $<

presenter.o : src/presenter.cxx  src/presenter.h  src/contract.h
	g++ $(CPPFLAGS) -Isrc/model -o $@ -c $<

clean:
	rm -f arty $(objects)

.PHONY : clean
```

اگر همه چیز درست پیش برود با اجرای دستور `make` در کنار این Makefile باید اجزای پروژه کامپایل شوند و یک برنامهٔ اجرایی به نام arty تولید شود. با اجرای این برنامه پنجره ای به شکل زیر نمایش داده می شود:

<figure><img src="../../images/arty-demo-program.png" style="width: 640px;"/><figcaption>نمونهٔ اجرای arty</figcaption></figure>

### مرحلهٔ دوم
#### انعطاف پذیری در پیکربندی
خیلی چیزها باید روی رایانهٔ شما درست باشند تا بتوانید خروجی تصویر پیشین را ببینید. مثل اینکه نسخهٔ ۲ کتابخانهٔ SDL را به همراه فایلهای سرایند مربوطه نصب کرده باشید؛ این کتابخانه را همانجایی نصب کرده باشید که من روی رایانهٔ خودم نصب کرده ام؛ کامپایلر ++C شما هم ++g باشد، و بسیاری موارد دیگر. این پروژه بسیار کوچک و ساده است، و تازه این مواردی است که من حدس می زنم ممکن است روی رایانهٔ شما متفاوت باشد. شاید کامپایل برنامه های ++C روی دستگاه شما به تنظیمات خاصی نیاز داشته باشد که من نتوانسته باشم پیشبینی کنم. این مسائل نشان می دهد استفاده از این Makefile به تنهایی از دید پیکربندی ماشین مقصد انعطاف پذیر نیست. شما برای این پروژه می توانید تنظیمات پیش پردازنده (preprocessor) و پیوندزن (linker) مناسب دستگاه خود را در متغیرهای `CPPFLAGS` و `LDFLAGS` قرار دهید؛ ولی تصور کنید برای یک پروژهٔ بزرگتر و پیچیده تر این کار چقدر سخت تر است. 

#### مدیریت وابستگی ها
 دومین کاستی مهم این روش مسالهٔ وابستگی ها است. من هنگام نوشتن این Makefile خودم را جای یک برنامه نویس متوسط گذاشتم و برای کشف وابستگی یک فایل cxx به سرایندهای مختلف، تنها به فهرست includes ابتدای همان فایل و سرایند مربوطه اش بسنده کردم. این کار باعث شد وابستگی presenter.o به line.h را نادیده بگیرم. همچنین تکراری بودن این فرایند در همین مقیاس کوچک باعث شد من اشتباه کنم و وابستگی presenter.o به stroke.h را نیز نادیده بگیرم. وقتی یکی از این دو سرایند را تغییر دهیم و بخواهیم برنامه را از نو کامپایل کنیم، این اشتباهات باعث می شوند make متوجه وابستگی presenter.o به سرایندهای مورد استفاده نشود و presenter.cxx را کامپایل نکند.
 
#### پشتیبانی از عملیات نصب
چه می شود اگر بخواهیم یک گزینه برای نصب نرم افزار خود روی رایانهٔ سایر کاربران فراهم کنیم؟ در محیط GNU به طور معمول برنامه های اجرایی در مسیر  /usr/local/bin نصب می شوند. اما یک Makefile خوب باید از متغیرهای استاندارد برای تعیین محل درست نصب برنامه های اجرایی و سایر فایلها استفاده کند. براساس راهنمای make، برنامه های اجرایی باید در ‍`$(exec_prefix)/bin` نصب شوند. البته به شرطی که متغیر exec_prefix مقدار درستی داشته باشد. برای مثال من با افزودن قاعدهٔ زیر به Makefile می خواستم امکان نصب را به شکلی ساده به این پروژه اضافه کنم:

```makefile
install : arty
	cp arty $(exec_prefix)/bin/arty
	
.PHONY : clean install
```
	

 ولی با اجرای ‍‍`make install` این خروجی تولید می شد:

```sh
cp arty /bin/arty
cp: cannot create regular file '/bin/arty': Permission denied
```

معلوم است که هنگام اجرای دستور، متغیر exec_prefix مقداری ندارد.

#### کامپایل در محلی غیر از مسیر پیشفرض
فرض کنیم بخواهید فایلهای باینری و اجرایی که از کامپایل به دست می آیند در محلی غیر از مسیر خود Makefile تولید شوند. یکی از مواقعی که چنین امکانی نیاز می شود وقتی است که کد منبع پروژه و Makefile در یک رسانهٔ فقط خواندنی مثل CD، سرویس FTP یا محلی از دیسک که در آنجا مجوز نوشتن ندارید ذخیره شده باشد. در این حالت شما یا دیگر کاربران نیاز دارید از یک مسیر در پوشه‌ٔ خانگی خود یا هرجا که مجوز نوشتن دارید عمل کامپایل را انجام دهید. موقعیت دیگر وقتی است که بخواهیم چند نسخه از نرم افزار خود را با پیکربندیهای مختلف کامپایل کنیم. برای مثال یک نسخه خطایابی شامل همهٔ اطلاعات debug و بدون بهینه سازی، و یک نسخهٔ نهایی بدون اطلاعات debug و بهینه سازی شده. در این حالت نیز لازم است نسخه های مختلف در مسیرهای مختلفی کامپایل شوند. حتما متوجه شده اید که کپی کردن Makefile به یک مسیر با مجوز نوشتن این مساله را حل نمی کند چون مسیر فایلهایی که باید کامپایل شوند به همراه مسیر جستجوی سرایندها در Makefile صریحا نوشته شده است. درنتیجه make نمی تواند فایلهای منبع لازم برای کامپایل هر فایل باینری را بیابد.
برنامهٔ‌ make امکاناتی برای حل این مسائل دارد. ما می توانیم متغیری به نام VPATH در Makefile داشته باشیم که شامل مسیرهای جستجوی پیشنیازها و اهداف است. البته برای منظور ما کافی است تنها شامل مسیر جستجوی وابستگی ها باشد. سپس باید در مسیرهای فایلهای وابستگی بخش src را برداریم. به این ترتیب make می تواند وابستگی ها را بیابد. ولی هنوز کامپایلر در یافتن سرایندها مشکل خواهد داشت. برای این منظور یک متغیر به نام SOURCE تعریف می کنیم که پوشهٔ ریشهٔ کد منبع را نشان دهد. سپس هم در VPATH و هم در CPPFLAGS از مقدار این متغیر و زیرشاخه های آن استفاده می‌کنیم.

برای روشن شدن موضوع یک پوشه به نام build در کنار شاخهٔ src ساختیم و Makefile زیر را در آن قرار دادیم:

```makefile
objects = stroke.o presenter.o sdlview.o main.o eventloop.o  

SOURCE = ../src

VPATH = $(SOURCE)

CPPFLAGS = -I/usr/include/SDL2 -I$(SOURCE) -I$(SOURCE)/model -I$(SOURCE)/graphics

LDFLAGS = -lSDL2


arty : $(objects)
	g++ -o arty $(objects) $(LDFLAGS)

eventloop.o : eventloop.cxx  eventloop.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

sdlview.o : graphics/sdlview.cxx  graphics/sdlview.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

main.o : main.cxx  presenter.h  graphics/sdlview.h  eventloop.h
	g++ $(CPPFLAGS) -o $@ -c $<

stroke.o : model/stroke.cxx  model/stroke.h  model/line.h
	g++ $(CPPFLAGS) -o $@ -c $<

presenter.o : presenter.cxx  presenter.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

clean:
	rm -f arty $(objects)

install : arty
	cp arty $(exec_prefix)/bin/arty

.PHONY : clean install
```
با استفاده از این تغییرات توانستیم arty را در شاخهٔ build کامپایل کنیم. اما همچنان مشکل انعطاف ناپذیری وجود دارد. یعنی برای استفاده از این روش در نرم افزار خود، باید Makefile اصلی ما به همین شکل باشد، با این تفاوت که مقدار متغیر SOURCE از پیش مشخص نیست. هر کاربری که نرم افزار ما را دریافت می کند باید این Makefile را در پوشهٔ دلخواه خود کپی کند و مقدار SOURCE را دستی تعیین کند.

تا اینجا ما تغییرات را در مرحلهٔ دوم کد منبع ذخیره کردیم. برای مشاهدهٔ این تغییرات دستور زیر را در خط فرمان در پوشهٔ کد منبع وارد کنید:

```shell
git checkout Stage2
```

### مرحلهٔ سوم
دیدیم که تنها با استفاده از make نمی توانیم پروژه را برای تنظیمات خاص رایانهٔ هر کاربر پیکربندی کنیم؛ مدیریت صحیح وابستگی ها هم تنها با make کاری بسیار سخت و شاید غیرممکن است. شاید اگر یک نویسندهٔ حرفه ای Makefile بودیم می‌توانستیم. ولی در ادامهٔ این آموزش روشهای بهتر و متداولتری برای انجام این کارها را یاد خواهیم گرفت.

#### استفاده از جانگهدار برای مقدار متغیرها
با درنظرگرفتن محدودیتهای make شاید به این نتیجه رسیده باشید که لازم است از یک ابزار دیگر استفاده کنیم. حدس شما کاملا درست است. ما قصد داریم مانند سایر برنامه نویسانی که در چنین موقعیتهایی قرار گرفته اند از یک اسکریپت پوسته (shell script) استفاده کنیم. این اسکریپت باید کارهای زیر را انجام دهد:

* وجود و عملکرد کامپایلر و کتابخانهٔ استاندارد ++C را آزمایش کند.
* از وجود کتابخانهٔ SDL2 مطمئن شود و مسیر فایلهای این کتابخانه را بیابد
* از هر مسیری که اجرا شد یک نسخه از Makefile در همان مسیر بسازد طوری که شامل همهٔ پارامترهای پیکربندی باشد. به علاوه این Makefile باید دارای مقدار صحیح متغیر SOURCE باشد.

ما این اسکریپت را *اسکریپت پیکربندی* می نامیم. فایل شامل این اسکریپت نیز به طور معمول configure نامیده می شود. پیش از شروع به نوشتن چنین اسکریپت پیچیده ای، ابتدا بیایید Makefile را تغییر دهیم. ما یک نمونه Makefile می خواهیم که کاربران نهایی بتوانند با استفاده از اسکریپتی که در مراحل بعدی می نویسیم Makefile هایی مخصوص ماشین خود از رویش بسازند. مقدار برخی از متغیرها را نمی توان به شکل معمولی تنظیم کرد، ولی باید راهی باشد که اسکریپت پیکربندی بداند مقدار هر متغیر را چطور تنظیم کند. پس Makefile اصلی پروژه باید شامل چند جانگهدار باشد. ما مانند سایر برنامه نویسان GNU از الگوی @xxx@ برای جانگهدارها استفاده می کنیم. چون make نمی تواند جانگهدارهایی که استفادهٔ خارجی دارند را بفهمد، نمونهٔ Makefile ما هم نمی تواند یک Makefile واقعی باشد. ما این فایل را به پیروی از روش متداول Makefile.in می نامیم.

حال می توانید Makefile.in را ببینید:
```makefile
objects = stroke.o presenter.o sdlview.o main.o eventloop.o  

SOURCE = @SOURCE@

VPATH = $(SOURCE)

CPPFLAGS = @CPPFLAGS@ -I$(SOURCE) -I$(SOURCE)/model -I$(SOURCE)/graphics

LDFLAGS = @LDFLAGS@

arty : $(objects)
	g++ -o arty $(objects) $(LDFLAGS)

eventloop.o : eventloop.cxx  eventloop.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

sdlview.o : graphics/sdlview.cxx  graphics/sdlview.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

main.o : main.cxx  presenter.h  graphics/sdlview.h  eventloop.h
	g++ $(CPPFLAGS) -o $@ -c $<

stroke.o : model/stroke.cxx  model/stroke.h  model/line.h
	g++ $(CPPFLAGS) -o $@ -c $<

presenter.o : presenter.cxx  presenter.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

clean:
	rm -f arty $(objects)

install : arty
	cp arty $(exec_prefix)/bin/arty

.PHONY : clean install
```

#### معرفی autoconf
اکنون می توانیم اولین نرم افزار بستهٔ autotools را معرفی کنیم. کار autoconf تولید اسکریپتهای پیکربندی است. پیش از ادامهٔ آموزش لازم است این برنامه و چند برنامهٔ وابسته را روی رایانهٔ خود داشته باشید. در خط فرمان بنویسید `autoconf -V` و اجرا کنید. باید یک خروجی به شکل زیر مشاهده کنید:

```text
autoconf (GNU Autoconf) 2.69
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+/Autoconf: GNU GPL version 3 or later
<http://gnu.org/licenses/gpl.html>, <http://gnu.org/licenses/exceptions.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by David J. MacKenzie and Akim Demaille.
```

ما با نوشتن یک فایل تقریبا کوتاه به نام configure.ac برنامه‌ٔ autoconf را راهنمایی می کنیم که اسکریپت پیکربندی را بسازد. این فایل شامل احضار ماکروهای autoconf است که به زبان m4 نوشته شده اند. فرایند استفاده از autoconf در ساده ترین حالت خود به شکل زیر است:

<figure>
<img src="../../images/autoconf-simple-usecase.png" style="width: 816px;"/>
<figcaption>نمودار کاربرد سادهٔ autoconf برای تولید اسکریپت پیکربندی</figcaption>
</figure>

#### طرز کار یک اسکریپت پیکربندی
کار اصلی این اسکریپت اجرای تعدادی آزمایش روی جنبه های معین سخت افزار و نرم افزار سیستم، تنظیم مقدار چند متغیر بر اساس نتایج آزمایشها، جاگذاری این مقادیر در یک یا چند فایل ورودی و تولید فایلهای خروجی است. فایل Makefile.in یک نمونه فایل ورودی است. این فایلها همگی شامل جانگهدارهایی با الگویی هستند که پیش از این تعریف کردیم. اگر اسکریپت پیکربندی یک متغیر خروجی به نام فرضی variable را تنظیم کند، برای جاگذاری مقدار آن در فایلهای خروجی باید جانگهداری به شکل @variable@ در فایل ورودی نظیر نوشته شود. 

یک عملکرد ویژهٔ اسکریپت پیکربندی تولید فایل config.h از روی ورودی config.h.in است. به شکل متداول config.h شامل تعریف ماکروهایی است که کامپایل برنامه های زبان C یا ++C را تحت تاثیر قرار می دهند. فایل config.h.in برخلاف سایر فایلهای ورودی شامل جانگهدارهایی به شکل @variable@ نیست. اصولا نیازی به نوشتن دستی این فایل نیست چون ابزار دیگر بستهٔ autotools به نام autoheader این فایل را برای ما از روی configure.ac تولید می کند.

#### نوشتن گام به گام configure.ac برای arty
در این مرحله یک فایل جدید به نام configure.ac ایجاد می کنیم. در این فایل باید تعدادی از ماکروهای autoconf را بنویسیم که پیکربندی arty را ممکن می سازند. خبر خوب این که لازم نیست تمام محتوای این فایل را خودمان بنویسم، چون حتی تولید چهارچوب اولیهٔ configure.ac نیز به شکل خودکار درآمده است. تنها لازم است در خط فرمان به مسیر ریشهٔ پروژهٔ arty بروید و دستور زیر را اجرا کنید:

```sh
$ autoscan
```

برنامهٔ autoscan فایلهای منبع پروژه را بررسی می کند و یک نمونهٔ ابتدایی از configure.ac مناسب این پروژه تولید می کند. این نمونه را در فایلی به نام configure.scan ذخیره می کند. پس ابتدا این فایل را تغییر نام می دهیم:

```sh
$ mv configure.scan configure.ac
```

بعد آن را در یک ویرایشگر باز می کنیم. اگر ویرایشگر شما قابلیت syntax highlighting دارد ولی نمی تواند زبان فایل را تشخیص دهد، خودتان زبان m4 را تعیین کنید. باید محتوای فایل مانند فهرست زیر باشد:

```m4
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT([FULL-PACKAGE-NAME], [VERSION], [BUG-REPORT-ADDRESS])
AC_CONFIG_SRCDIR([src/main.cxx])
AC_CONFIG_HEADERS([config.h])

# Checks for programs.
AC_PROG_CXX
AC_PROG_CC
AC_PROG_INSTALL

# Checks for libraries.

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.
AC_CHECK_HEADER_STDBOOL
AC_TYPE_SIZE_T

# Checks for library functions.

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
```

خطوطی که با # شروع شده اند توضیحات هستند. سایر خطوط احضار ماکروهای autoconf است. چهار ماکروی اول وقتی بسط بیابند کدی را تولید می کنند که بین همه‌ٔ اسکریپتهای پیکربندی مشترک است، و نشان می دهند این اسکریپت پیکربندی متعلق به پروژهٔ arty است. این ماکروها به ترتیب کارهای زیر را انجام می دهند:

##### ماکروی AC_PREREQ
این ماکرو اعلام می کند ما قصد داریم از کدام نسخهٔ autoconf استفاده کنیم. برای مثال ‍`AC_PREREQ([2.69])` یعنی از autoconf نسخه‌‌ٔ ۲.۶۹ یا نسخه های جدیدتر برای پردازش این فایل configure.ac استفاده شود. نسخه های قبل از ۲.۶۹ هنگام پردازش این فایل یک پیام خطا چاپ می کنند و پردازش را ادامه نمی دهند.

##### ماکروی AC_INIT
بسط این ماکرو پارامترهای خط فرمان اسکریپت را پردازش می کند و کارها و بررسی های اولیهٔ مختلفی انجام می دهد. همچنین در این ماکرو نام بسته‌ٔ نرم افزاری و نسخهٔ آن، و در صورت تمایل آدرس تماسی را می نویسیم که کاربران بتوانند برای گزارش اشکالات به آنجا مراجعه کنند. برای پروژهٔ arty این خط را به شکل زیر تغییر می دهیم:

```m4
AC_INIT([arty], [0.1])
```

##### ماکروی AC_CONFIG_SRCDIR
این ماکرو کدی را بسط می دهد که وجود فایل تعیین شده را در زمان اجرا بررسی می کند. اگر این فایل در مسیر تعیین شده وجود داشته باشد به این معنی است که اسکریپت configure درجای درستی قرار گرفته است.

##### ماکروی AC_CONFIG_HEADERS
این ماکرو کد تولید فایلهای سرایند پیکربندی را ایجاد می‌کند. در این مثال ‍`AC_CONFIG_HEADERS([config.h])` می گوید یک سرایند پیکربندی به نام config.h از روی config.h.in تولید شود.

##### ماکروهای بررسی وجود و عملکرد برنامه ها
سه ماکروی `AC_PROG_CC` ، `AC_PROG_CXX` و `AC_PROG_INSTALL` وجود و درستی عملکرد کامپایلر ++C، کامپایلر C و نصب کننده‌ٔ پیشفرض را بررسی می کنند.

ماکروی `AC_PROG_CC` یک کامپایلر C قابل استفاده می یابد. اگر متغیر محیطی CC تعریف نشده باشد، gcc، cc و بعد سایر کامپایلر ها را امتحان می کند. در انتها متغیر خروجی `CC` را با نام و مسیر کامپایلر یافت شده تنظیم می کند. اگر اسکریپت پیکربندی تشخیص داد از gcc استفاده کند، متغیر اسکریپتی `GCC` را با مقدار `yes` تنظیم میکند. همچنین اگر مقدار متغیر خروجی `CFLAGS` قبلا تعیین نشده باشد، آن را با `g O2-` یا تنها با `O2` تنظیم می کند. اگر این مقدار پیشفرض مناسب نرم افزارتان نیست  (که برای هیچکدام از پروژه های من مناسب نبوده است)، خط زیر را بعد از ‍‍`AC_INIT` و قبل از ‍‍`AC_PROG_CC` درج کنید:

```m4
: ${CFLAGS=""}
```

ماکروی `AC_PROG_CXX` مشابه کار `AC_PROG_CC` را برای یافتن یک کامپایلر ++C انجام می دهد. آدرس کامپایلر یافت شده در متغیر خروجی `CXX` ذخیره می شود. اگر این ماکرو تشخیص داد که از کامپایلر gnu استفاده کند، متغیر اسکریپتی `GXX` را با مقدار `yes` تنظیم میکند. همچنین اگر مقدار متغیر خروجی `CXXFLAGS` قبلا تعیین نشده باشد، آن را با `g O2-` یا تنها با `O2` تنظیم می کند. اگر این مقدار پیشفرض مناسب نرم افزار شما نیست خط زیر را بعد از ‍‍`AC_INIT` و قبل از ‍‍`AC_PROG_CXX` درج کنید:

```m4
: ${CXXFLAGS=""}
```

ماکروی `AC_PROG_INSTALL` یک برنامهٔ نصب کنندهٔ سازگار با BSD را روی سیستم کاربر می یابد و متغیر خروجی `INSTALL` را با مسیر و نام آن برنامه تنظیم می‌کند. اگر برنامهٔ نصب کنندهٔ مناسبی یافت نشد مقدار `dir/install-sh -c` را در این متغیر ذخیره می کند. ما عملیات نصب را در بخشهای آینده آموزش خواهیم داد. پس اکنون خط شامل این ماکرو را به توضیحات تبدیل می کنیم.

##### ماکروهایی که کتابخانه های نرم افزاری را بررسی می کنند
بخش بعدی `configure.ac` محل نوشتن ماکروهایی است که هنگام بسط یافتن کد آزمایش کتابخانه های نرم افزاری را تولید می کنند. برای بررسی هر یک از  فایلهای `lib*.so` و ‍‍`lib*.a` که نرم افزار ما روی سیستم کاربر استفاده می ‌کند باید یک ماکروی مناسب در این قسمت نوشته شود. ما در آینده ماکرویی برای بررسی وجود کتابخانهٔ SDL2 به این بخش اضافه می کنیم.

##### ماکروهایی برای بررسی وجود سرایندها
در این بخش ماکروهایی را می نویسیم که وجود سرایندهای مهم کتابخانه ای را روی سیستم کاربر بررسی می کنند. ما در بخشهای آینده ماکرویی برای بررسی وجود سرایند SDL.h به این بخش می افزاییم.

##### ماکروهایی برای بررسی انواع داده ها، ساختمانها و خصوصیات کامپایلر
کامپایلرهای مختلف یا حتی نسخه های مختلف یک کامپایلر جزییات عملکرد متفاوتی دارند. گاهی برنامه نویس از نوع داده ای در برنامه اش استفاده می‌کند که روی همهٔ کامپایلرها یا در همهٔ کتابخانه های استاندارد پشتیبانی نمی شود. ما در این بخش `configure.ac` ماکروهایی را می نویسیم که وجود این انواع داده را بررسی می کنند، اگر انواع دادهٔ مورد نیاز در سیستم کاربر پشتیبانی نمی شوند جایگزینهایی را تعریف می کنند و در نهایت اگر هیچ جایگزینی وجود نداشت پیغام خطا می‌دهند. ‍‍`autoscan` تشخیص داده که پروژهٔ arty به دوتا از این ماکروها نیاز دارد:

* ماکروی `AC_CHECK_HEADER_STDBOOL` وجود سرایند ‍‍`stdbool.h` سازگار با C99 را روی سیستم هدف بررسی می کند. اگر اسکریپت پیکربندی بتواند این سرایند را بیابد ماکروی ‍`HAVE_STDBOOL_H` را با مقدار ۱ در فایل `config.h` تعریف می کند. چون ما در کد arty از نوع `bool` استفاده کرده ایم  ‍‍`autoscan` تشخیص داده لازم است از این سرایند استفاده کنیم.
* ماکروی `AC_TYPE_SIZE_T` بررسی می کند که کامپایلر یا سرایندهای استاندارد سیستم هدف نوع `size_t` را تعریف کنند، و در صورت نیاز خودش این نوع را به شکل مناسب تعریف می کند. این تعریف در فایل `config.h` قرار خواهد گرفت.

##### ماکروهایی برای بررسی توابع کتابخانه ای
در این بخش ماکروهایی را می‌نویسیم که هنگام بسط یافتن وجود و عملکرد صحیح توابع کتابخانه ای را روی سیستم کاربر بررسی می کنند. ما در ادامهٔ این آموزش ماکروهایی در این بخش می‌نویسیم که وجود توابع ‍‍`SDL_InitSubSystem` و `SDL_CreateWindow` را بررسی کنند.

##### آماده سازی و تولید خروجی
در آخرین بخش ماکروهایی را می نویسیم که فایلهای خروجی اسکریپت پیکربندی را تعریف می کنند و درنهایت این فایلها را تولید می‌کنند. ماکروی `AC_CONFIG_FILES` نام و مسیر فایلهای خروجی را تعیین می کند. اولین پارامتر این ماکرو فهرست فایلهای خروجی است که با جای خالی از هم جدا می‌شوند. در ازای هر فایلی که در این فهرست می نویسید باید یک فایل ورودی با پسوند `in` در زیرشاخهٔ نظیر فهرست کد منبع وجود داشته باشد. همه‌ٔ این فایلهای خروجی با جاگذاری متغیرهای خروجی در جانگهدارهای فایلهای ورودی تولید می شوند.

ماکروی ‍‍`AC_OUTPUT` کد تولید فایلهای خروجی را تولید می کند و باید در انتهای هر فایل `configure.ac` نوشته شود.

#### تولید فایل ورودی config.h.in
کافی است در خط فرمان در مسیر ریشهٔ پروژه دستور `autoheader` را اجرا کنید. محتوای ‍‍`config.h.in` مانند فهرست زیر خواهد بود:

```C
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <inttypes.h> header file. */
#undef HAVE_INTTYPES_H

/* Define to 1 if you have the <memory.h> header file. */
#undef HAVE_MEMORY_H

/* Define to 1 if you have the <stdint.h> header file. */
#undef HAVE_STDINT_H

/* Define to 1 if you have the <stdlib.h> header file. */
#undef HAVE_STDLIB_H

/* Define to 1 if you have the <strings.h> header file. */
#undef HAVE_STRINGS_H

/* Define to 1 if you have the <string.h> header file. */
#undef HAVE_STRING_H

/* Define to 1 if you have the <sys/stat.h> header file. */
#undef HAVE_SYS_STAT_H

/* Define to 1 if you have the <sys/types.h> header file. */
#undef HAVE_SYS_TYPES_H

/* Define to 1 if you have the <unistd.h> header file. */
#undef HAVE_UNISTD_H

/* Define to 1 if the system has the type `_Bool'. */
#undef HAVE__BOOL

/* Define to the address where bug reports for this package should be sent. */
#undef PACKAGE_BUGREPORT

/* Define to the full name of this package. */
#undef PACKAGE_NAME

/* Define to the full name and version of this package. */
#undef PACKAGE_STRING

/* Define to the one symbol short name of this package. */
#undef PACKAGE_TARNAME

/* Define to the home page for this package. */
#undef PACKAGE_URL

/* Define to the version of this package. */
#undef PACKAGE_VERSION

/* Define to 1 if you have the ANSI C header files. */
#undef STDC_HEADERS

/* Define to `unsigned int' if <sys/types.h> does not define. */
#undef size_t
```
می بینید که این فایل شامل راهنماهای پیش پردازنده برای لغو تعریف ماکروهایی است که قرار است در کد C و ++C استفاده شوند. اسکریپت پیکربندی هنگامی که می خواهد فایل config.h را از روی فایل config.h.in بسازد، بر اساس شرایط زمان اجرا برخی از این ماکروها را با مقدار مناسب تعریف می کند. لغو شدن تعریف این ماکروها به شکل پیشفرض در config.h.in به ما اطمینان می‌دهد اگر این ماکروها (یا ماکروهایی همنام آنها) در جای دیگر تعریف شده و بر اساس شرایطی که در configure.ac تعیین کرده ایم نباید تعریف می شدند، با رسیدن به سرایند config.h لغو شوند.

#### تولید اسکریپت پیکربندی
این کار هم بسیار ساده است. در خط فرمان در مسیر ریشهٔ پروژه دستور ‍‍`autoconf` را اجرا کنید. اگر همه چیز درست باشد فایلی به نام configure با مجوز اجرا در همین مسیر ساخته می شود. ما محتویات این فایل را در این مقاله نشان نمی دهیم چون حجمش زیاد است. برای اینکه مطمئن شوید این اسکریپت بدون مشکل تولید شده در خط فرمان دستور ‍‍`configure --version/.` را اجرا کنید. باید خروجی به شکل زیر تولید شود:

```text
arty configure 0.1
generated by GNU Autoconf 2.69

Copyright (C) 2012 Free Software Foundation, Inc.
This configure script is free software; the Free Software Foundation
gives unlimited permission to copy, distribute and modify it.
```

حال ببینیم این اسکریپت چه پارامترهایی را از خط فرمان می پذیرد. دستور `configure --help/.` را اجرا کنید. خروجی زیر را خواهید دید:

```text
`configure' configures arty 0.1 to adapt to many kinds of systems.

Usage: ./configure [OPTION]... [VAR=VALUE]...

To assign environment variables (e.g., CC, CFLAGS...), specify them as
VAR=VALUE.  See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Configuration:
  -h, --help              display this help and exit
      --help=short        display options specific to this package
      --help=recursive    display the short help of all the included packages
  -V, --version           display version information and exit
  -q, --quiet, --silent   do not print `checking ...' messages
      --cache-file=FILE   cache test results in FILE [disabled]
  -C, --config-cache      alias for `--cache-file=config.cache'
  -n, --no-create         do not create output files
      --srcdir=DIR        find the sources in DIR [configure dir or `..']

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
                          [/usr/local]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
                          [PREFIX]

By default, `make install' will install all the files in
`/usr/local/bin', `/usr/local/lib' etc.  You can specify
an installation prefix other than `/usr/local' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

Fine tuning of the installation directories:
  --bindir=DIR            user executables [EPREFIX/bin]
  --sbindir=DIR           system admin executables [EPREFIX/sbin]
  --libexecdir=DIR        program executables [EPREFIX/libexec]
  --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
  --libdir=DIR            object code libraries [EPREFIX/lib]
  --includedir=DIR        C header files [PREFIX/include]
  --oldincludedir=DIR     C header files for non-gcc [/usr/include]
  --datarootdir=DIR       read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR           read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR           info documentation [DATAROOTDIR/info]
  --localedir=DIR         locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR            man documentation [DATAROOTDIR/man]
  --docdir=DIR            documentation root [DATAROOTDIR/doc/arty]
  --htmldir=DIR           html documentation [DOCDIR]
  --dvidir=DIR            dvi documentation [DOCDIR]
  --pdfdir=DIR            pdf documentation [DOCDIR]
  --psdir=DIR             ps documentation [DOCDIR]

Some influential environment variables:
  CXX         C++ compiler command
  CXXFLAGS    C++ compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  LIBS        libraries to pass to the linker, e.g. -l<library>
  CPPFLAGS    (Objective) C/C++ preprocessor flags, e.g. -I<include dir> if
              you have headers in a nonstandard directory <include dir>
  CC          C compiler command
  CFLAGS      C compiler flags
  CPP         C preprocessor

Use these variables to override the choices made by `configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to the package provider.
```

ما هنوز ماکروهایی خاص پروژهٔ arty به ‍configure.ac اضافه نکرده ایم. آنچه در دو خروجی قبل می بینید رفتار مشترک همه‌ٔ اسکریپت های پیکربندی است.

#### افزودن ماکروهای خاص پروژه‌ٔ arty به configure.ac
مهمترین نیازمندی پروژهٔ arty بعد از کامپایلر و کتابخانهٔ استاندارد ++C کتابخانهٔ libSDL2 به همراه سرایندهایی است که کار توسعه بر اساس این کتابخانه را ممکن می سازند. ما در این بخش ماکروهایی به configure.ac می افزاییم که وجود این موارد و همچنین وجود تابع کتابخانه ای `SDL_CreateWindow` را روی سیستم کاربران بررسی می کنند.

یکی از مسائل مشترک ما برنامه نویسان هنگام استفاده از کتابخانه های غیر استاندارد یافتن محل نصب فایلهای آنها در ماشین هدف است. در طی این آموزش نیز برای یافتن آدرس کتابخانهٔ libSDL2.so و فایلهای سرایند مربوطه با مشکلاتی مواجه خواهیم شد. اگرچه معمولا نرم افزارهای مدیریت بستهٔ یک سیستم کتابخانه ها و سرایندها را در مسیرهای استانداردی نصب می کند، ولی گاهی اوقات بر حسب نیاز لازم است فایلها در مسیرهای دیگری نصب شوند. برای اینکه در زمان اجرای اسکریپتهای پیکربندی مشخص شود آدرس نهایی فایلهای نصبی نرم افزارها و کتابخانه ها کجا است، توسعه دهندگان این نرم افزارها از روشهای مختلفی استفاده می کنند. دو روش استفاده از pkg-config و استفاده از یک اسکریپت فراهم کننده‌ٔ آدرسهای نصب بیش از همه متداول است.

در روش اول وقتی یک نرم افزار روی سیستم کاربر نصب می شود، علاوه بر فایلهای معمولی یک فایل با پسوند `pc.` نیز در مسیری نصب می شود که برنامه‌ٔ pkg-config بتواند آن را پیدا کند. برای مثال روی سیستم من این فایلها در آدرس usr/share/pkgconfig/ قرار دارند. این فایلها دارای ساختار معینی هستند و آدرسها مختلف نصب نرم افزار مربوط به خود را دارند. برنامهٔ pkg-config این فایلها را می خواند و بر اساس پارامترهای خط فرمانی که به آن داده ایم برخی از آدرسها را چاپ می کند.

در روش دوم یک اسکریپت در مسیر برنامه های اجرایی، برای مثال در usr/bin/ نصب می شود که شامل همه‌ٔ آدرسهای نصب و سایر پارامترهایی است که ممکن است سایر توسعه دهندگان هنگامی که از این نرم افزار استفاده می کنند، به آنها نیاز داشته باشند. این اسکریپت با گرفتن پارامترهای مناسب از خط فرمان، این آدرسها و پارامترها را چاپ می‌کند.

کتابخانهٔ SDL2 از روش دوم استفاده می‌کند. بر اساس [صفحهٔ رهنمای نصب SDL2 در وبسایت libSDL](http://wiki.libsdl.org/Installation) این کتابخانه در زمان نصب اسکریپتی با نام sdl2-config را در مسیر اجرایی نصب می کند. میتوانیم با اجرای ‍‍`which sdl2-config` از وجود آن مطمئن شویم. حال ببینیم این اسکریپت چه اطلاعاتی به ما می دهد:

```text
$ sdl2-config --help
Usage: /usr/bin/sdl2-config [--prefix[=DIR]] [--exec-prefix[=DIR]] [--version] [--cflags] [--libs] [--static-libs]
```

اگر این اسکریپت را با پارامتر `cflags--` صدا بزنیم تنظیماتی را نشان خواهد داد که باید در متغیر ‍`CPPFLAGS` قرار بگیرند. اگر آن را با `libs--` صدا بزنیم پارامترهای linker را چاپ می‌کند که باید در متغیر `LIBS` قرار بگیرند.

##### آزمایش وجود کتابخانهٔ libSDL2
برای این کار از ماکروی `AC_CHECK_LIB` استفاده می کنیم. روش احضار این ماکرو به شکل زیر است:

```m4
AC_CHECK_LIB(library, function, [action-if-found], [action-if-not-found], [other-libraries])
```

این ماکرو در دسترس بودن کتابخانه ای به نام `library` را بررسی می کند، و این کار را با کامپایل، پیوند زدن و اجرای یک برنامهٔ آزمایشی که از تابع `function` استفاده می‌کند انجام می دهد. نام کتابخانه باید تنها به شکل پایه ذکر شود. برای مثال برای کتابخانه‌ٔ libSDL2 باید اولین آرگومان این ماکرو ‍‍`SDL2` باشد. ما قصد داریم از تابع `SDL_Quit` به عنوان تابع مورد آزمایش استفاده کنیم.

سه آرگومان بعدی این ماکرو اختیاری اند. `action-if-found` فهرست دستورهای پوسته ای است که در صورت یافت شدن کتابخانه اجرا می شوند. اگر هیچ دستوری تعیین نشود به شکل پیشفرض ‍‍`llibrary-` به ابتدای متغیر خروجی `LIBS` افزوده می شود. افزودن به ابتدای ‍`LIBS` به دلیل حساسیت linker به ترتیب کتابخانه ها است. اگر یک اسکریپت پیکربندی وجود چند کتابخانهٔ مختلف را آزمایش کند، معمولا کتابخانه هایی که در انتها آزمایش می‌شوند به کتابخانه هایی که قبلا آزمایش شدند وابسته اند.

به یاد دارید که برنامهٔ sdl2-config مقادیر لازم برای متغیر LIBS را چاپ می کند. پس دیگر نیازی نیست ماکروی `AC_CHECK_LIB` این متغیر را تغییر دهد. در عوض ما پیش از احضار `AC_CHECK_LIB` مقادیر لازم را از sdl2-config گرفته و به ابتدای LIBS اضافه می‌کنیم:

```sh
LIBS="`sdl2-config --libs` $LIBS"
```

آرگومان `action-if-not-found` فهرست دستورهایی را تعریف می کند که در صورت یافت نشدن کتابخانهٔ مورد آزمایش اجرا می شوند. در این مثال چون وجود libSDL2 برای کارکرد arty ضروری است، ما می خواهیم دستوری در این بخش بنویسیم که یک پیغام خطای مناسب چاپپ کند و اجرای اسکریپت پیکربندی را متوقف کند. `autoconf` برای این منظور ماکروهای مناسبی دارد. یکی از این ماکروها `AC_MSG_ERROR` است. این ماکرو را به شکل زیر استفاده می‌کنیم:

```m4
AC_MSG_ERROR(error-description, [exit-status = ‘$?/1’])
```

این ماکرو رشتهٔ `error-description` را در خروجی اسکریپت می نویسد و اجرای اسکریپت را تمام می کند. اگر `exit-status` ذکر شود اسکریپت پیکربندی این مقدار نتیجه را به سیستم عامل باز می گرداند.

آرگومان `other-libraries` در ماکروی `AC_CHECK_LIB` سایر کتابخانه های مورد نیاز پیوند زدن برنامهٔ آزمایشی به کتابخانهٔ مورد آزمایش را نشان می دهد. در مثال ما برای پیوند به libSDL2 به کتابخانهٔ دیگری نیاز نیست. بنابر این نیازی به ذکر این آرگومان نداریم.

در مجموع ما به شکل زیر اسکریپت پیکربندی را وامی داریم وجود کتابخانهٔ libSDL2 را بررسی کند، و اگر این کتابخانه را نیافت پیغام خطا نشان دهد:

```m4
LIBS="`sdl2-config --libs` $LIBS"
AC_CHECK_LIB(SDL2, SDL_Quit, [:], 
	[AC_MSG_ERROR([This program needs libSDL2, but the configure script couldn't find this library. Please install libSDL2 first and then retry])])
```

##### استفاده از مقدار متغیرهای اسکریپت پیکربندی در فایلهای خروجی
برای اینکه مقدار متغیر `LIBS` در دسترس Makefile باشد، باید در Makefile.in متغیری به نام LIBS تعریف کنیم و مقدار آن را با استفاده از یک جانگهدار تعیین کنیم. وقتی اسکریپت پیکربندی بخواهد فایل خروجی Makefile را بسازد، مقدار متغیر `LIBS` را جایگزین عبارت ‍‍`@LIBS@` خواهد کرد. همچنین باید از مقدار این متغیر در دستور پیوندزدن arty استفاده کنیم. تغییرات لازم Makefile.in به شکل زیر خواهد بود:

```makefile
LIBS = @LIBS@

arty : $(objects)
	g++ -o arty $(objects) $(LDFLAGS) $(LIBS)
```

در حالت کلی برای اینکه مقدار یک متغیر در فایلهای خروجی جایگزین شود، باید با ماکروی `AC_SUBST` اعلام کنیم آن متغیر، یک متغیر خروجی است. برخی متغیرهای استاندارد از پیش به عنوان متغیر خروجی تعریف شده اند. [فهرست این متغیرها در این صفحه قرار دارد](https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.69/html_node/Preset-Output-Variables.html#Preset-Output-Variables). می بینید که `LIBS` هم از پیش به عنوان متغیر خروجی تعریف شده است.

##### بررسی وجود و یافتن آدرس سرایندهای SDL
برای آزمایش وجود یک فایل سرایند ماکروی `AC_CHECK_HEADER` را به کار می بریم. این ماکرو در حالت ساده به شکل زیر استفاده می شود:

```m4
AC_CHECK_HEADER(header-file, [action-if-found], [action-if-not-found], [includes])
```

این ماکرو به طور پیشفرض هم وجود یک سرایند را با کمک پیش پردازنده آزمایش می کند، و هم با کامپایل یک برنامهٔ آزمایشی به زبان C قابل استفاده بودن این سرایند را آزمایش می کند. پارامتر `header-file` نام یک فایل سرایند است، برای مثال `SDL.h`. فهرست `action-if-found` فرمانهایی را نشان می دهد که در صورت پیدا شدن این سرایند و قابل استفاده بودن آن اجرا می شوند. فهرست فرمانهای ‍‍`action-if-not-found` وقتی اجرا می شوند که این سرایند پیدا نشود یا قابل استفاده نباشد. پارامتر `includes` فهرست خطوط `include#` را نشان می دهد که باید پیش از `include#` کردن سرایند مورد آزمایش در یک برنامه‌ٔ زبان C ذکر شود، تا آن برنامه به درستی کامپایل شود. اگر این پارامتر تعیین نشود، اسکریپت پیکربندی از [مقدار پیشفرض AC_INCLUDES_DEFAULT](https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.69/html_node/Default-Includes.html#Default-Includes) استفاده خواهد کرد.

مقدار متغیر ‍‍`CPPFLAGS` روی عملکرد این ماکرو تاثیر می‌گذارد. یکی از کاربردهای این متغیر تعیین آدرس جستجوی سرایندهایی است که ممکن است در آدرسهای جستجوی پیشفرض یافت نشوند. فایلهای سرایند کتابخانهٔ SDL 2 معمولا در usr/include/SDL2/ قرار دارند. ولی همیشه اینطور نیست. ممکن است در بعضی رایانه ها در مسیر usr/local/SDL2/ باشند. همانطور که پیش از این گفتیم اگر برنامهٔ sdl2-config را با پارامتر خط فرمان `cflags--` صدا بزنیم پارامترهای لازم برای یافتن سرایندهای این کتابخانه را چاپ می کند. پس ما باید پیش از استفاده از `AC_CHECK_HEADER` آدرس سرایندهای SDL2 را در متغیر ‍‍`CPPFLAGS` قرار دهیم، و در Makefile.in نیز از این متغیر استفاده کنیم.

در مجموع ما به این روش وجود سرایند SDL.h را آزمایش می کنیم:

```m4
# Checks for header files.

CPPFLAGS="$CPPFLAGS `sdl2-config --cflags`"

AC_CHECK_HEADER(SDL.h, [], [AC_MSG_ERROR([This program needs SDL.h which is part of libSDL2, but the configure script couldn't find this header file. Please install libSDL2 first and then retry])])
```

##### آزمایش وجود و عملکرد صحیح توابع کتابخانه ای `SDL_CreateWindow`
یکی از اختلافات مهم نسخه های ۱ و ۲ کتابخانهٔ SDL افزوده شدن امکان ساخت چند پنجره در SDL 2 است. تابع `SDL_CreateWindow` در این کتابخانه پنجره های جدید را می سازد. ما برای اینکه مطمئن باشیم برنامهٔ arty از نسخهٔ درست SDL استفاده میکند، در اسکریپت پیکربندی کدی را قرار میدهیم که وجود این تابع را بررسی کند، و همانطور که انتظار می‌رود باید یک ماکروی مخصوص به configure.ac اضافه کنیم. ماکروی `AC_CHECK_FUNC` وجود یک تابع زبان C را در کتابخانه های نصب شده بر ماشین مقصد بررسی می کند. تعریف این ماکرو به شکل زیر است:

```m4
AC_CHECK_FUNC(function, [action-if-found], [action-if-not-found])
```

اگر تابع function در دسترس باشد،فرمانهای پوستهٔ action-if-found اجرا می شوند، در غیر این صورت فرمانهای action-if-not-found اجرا خواهند شد. ما میخواهیم اگر SDL_CreateWindow در دسترس نیست، اسکریپت پیکربندی یک پیغام خطا نشان بدهد و متوقف شود. در نتیجه در configure.ac می نویسیم:

```m4
AC_CHECK_FUNC(SDL_CreateWindow, [], [AC_MSG_ERROR([SDL library installed on this system doesn't offer SDL_CreateWindow which is required by arty])])
```

##### تعیین مقداری برای متغیر SOURCE
به یاد دارید که ما از متغیری به نام SOURCE در Makefile و بعد از آن در Makefile.in استفاده کردیم تا بدانیم شاخهٔ ریشهٔ کد منبع کجا است. در Makefile.in جانگهداری به شکل `@SOURCE@` گذاشتیم، و قرار شد اسکریپت پیکربندی آدرس کد منبع را در این جانگهدار جایگزین کند. برای این منظور باید متغیری به نام SOURCE در اسکریپت پیکربندی با مقدار مناسب تعریف کنیم، و از اسکریپت پیکربندی بخواهیم آن را در جانگهدارهایی که در فایلهای ورودی می‌یابد جایگزین کند. برای این کار این دو خط را به configure.ac اضافه می کنیم:

```m4
SOURCE="$srcdir/src"
AC_SUBST(SOURCE)
```

متغیر `srcdir` توسط همهٔ اسکریپتهای پیکربندی تعریف می شود و ریشهٔ کدمنبع را نشان می دهد. مقدار این متغیر به شکل پیشفرض همان پوشه‌ٔ اسکریپت پیکربندی است. با استفاده از پارامتر خط فرمان `srcdir--` در زمان اجرای configure می توان این آدرس را تغییر داد. ماکروی ‍‍`AC_CONFIG_SRCDIR` از این آدرس استفاده می کند تا مطمئن شود اسکریپت پیکربندی در جای مناسبی از ساختار درخت کدمنبع قرار گرفته است.

##### شکل نهایی configure.ac در این مرحله
این فایل تا اینجا به شکل زیر است:

```m4
#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT([arty],[0.1])
AC_CONFIG_SRCDIR([src/main.cxx])
AC_CONFIG_HEADERS([config.h])

: ${CFLAGS=""}
: ${CXXFLAGS=""}

# Checks for programs.
AC_PROG_CXX
AC_PROG_CC
dnl AC_PROG_INSTALL

# Checks for libraries.
LIBS="`sdl2-config --libs` $LIBS"
AC_CHECK_LIB(SDL2, SDL_Quit, [:], [AC_MSG_ERROR([This program needs libSDL2, but the configure script couldn't find this library. Please install libSDL2 first and then retry])])

# Checks for header files.
CPPFLAGS="$CPPFLAGS `sdl2-config --cflags`"
AC_CHECK_HEADER(SDL.h, [], [AC_MSG_ERROR([This program needs SDL.h which is part of libSDL2, but the configure script couldn't find this header file. Please install libSDL2 first and then retry])])

# Checks for typedefs, structures, and compiler characteristics.
AC_CHECK_HEADER_STDBOOL
AC_TYPE_SIZE_T

# Checks for library functions.
AC_CHECK_FUNC(SDL_CreateWindow, [], [AC_MSG_ERROR([SDL library installed on this system doesn't offer SDL_CreateWindow which is required by arty])])

# Sets some output variables
SOURCE="$srcdir/src"
AC_SUBST(SOURCE)

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
```

##### شکل نهایی Makefile.in در این مرحله

```makefile
objects = stroke.o presenter.o sdlview.o main.o eventloop.o  

SOURCE = @SOURCE@

VPATH = $(SOURCE)

CPPFLAGS = @CPPFLAGS@ -I$(SOURCE) -I$(SOURCE)/model -I$(SOURCE)/graphics

LIBS = @LIBS@

arty : $(objects)
	g++ -o arty $(objects) $(LIBS)

eventloop.o : eventloop.cxx  eventloop.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

sdlview.o : graphics/sdlview.cxx  graphics/sdlview.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

main.o : main.cxx  presenter.h  graphics/sdlview.h  eventloop.h
	g++ $(CPPFLAGS) -o $@ -c $<

stroke.o : model/stroke.cxx  model/stroke.h  model/line.h
	g++ $(CPPFLAGS) -o $@ -c $<

presenter.o : presenter.cxx  presenter.h  contract.h
	g++ $(CPPFLAGS) -o $@ -c $<

clean:
	rm -f arty $(objects)

.PHONY : clean install
```

با اجرای دستور `autoconf` اسکریپت پیکربندی را بر اساس آخرین تغییرات configure.ac می سازیم. حال اسکریپت پیکربندی جدید مولفه های سیستم هدف را بررسی می کند و دو فایل config.h و Makefile را تولید می کند. اگر همه‌ٔ مراحل آموزش را دنبال کرده باشید، اجرای این اسکریپت در این مرحله خروجی زیر را تولید خواهد نمود:

```text
$ ./configure
checking for g++... g++
checking whether the C++ compiler works... yes
checking for C++ compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C++ compiler... yes
checking whether g++ accepts -g... yes
checking for gcc... gcc
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking for SDL_Quit in -lSDL2... yes
checking how to run the C preprocessor... gcc -E
checking for grep that handles long lines and -e... /usr/bin/grep
checking for egrep... /usr/bin/grep -E
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking SDL.h usability... yes
checking SDL.h presence... yes
checking for SDL.h... yes
checking for stdbool.h that conforms to C99... yes
checking for _Bool... yes
checking for size_t... yes
checking for SDL_CreateWindow... yes
configure: creating ./config.status
config.status: creating Makefile
config.status: creating config.h
config.status: config.h is unchanged
```

می توانید آزمایشهای مختلفی را تشخیص دهید که اسکریپت پیکربندی اجرا می کند. هربار که این اسکریپت را اجرا کنید همین آزمایشها تکرار خواهند شد. این اسکریپت با هربار اجرا شدنش علاوه بر فایلهای خروجی که ما تعیین کرده ایم، دو فایل دیگر را نیز می سازد یا بروز رسانی میکند. فایل config.log حاوی جزییات تمام مراحل اجرای اسکریپت configure و همچنین شامل نتایج همهٔ آزمایشها است. فایل config.status یک اسکریپت پوستهٔ دیگر است که کارش تولید فایلهای خروجی بر اساس پارامترها و نتیج آزمایشهای آخرین بار اجرای configure است. با استفاده از این اسکریپت config.status می توانیم در اجرای آزمایشهایی که configure باید انجام دهد صرفه جویی کنیم. برای مثال بعد از یک بار اجرا شدن configure روی یک رایانه، تغییراتی در Makefile.in بدهیم و config.status را اجرا کنیم تا یک Makefile جدید برای همین پیکربندی تولید کند.

##### کامپایل در محلی غیر از شاخهٔ کد منبع

اسکریپت پیکربندی که تا کنون تولید کرده ایم از همین حالا این قابلیت جالب را دارد که می توانیم آن را از هر مسیری روی رایانه‌ٔ مقصد صدا بزنیم،و این اسکریپت یک Makefile در همان شاخه تولید می کند. این Makefile می تواند پروژهٔ arty را در همین شاخه کامپایل کند. برای بررسی این قابلیت فرض کنیم کد منبع arty در مسیر src/arty/~ قرار دارد. در خط فرمان وارد کنید:

```sh
$ cd ~
$ mkdir build && cd build
$ ~/src/arty/configure
⁞
$ make
```

به انتهای این بخش از آموزش رسیدیم. می توانید با اجرای دستور زیر، کد منبع را به انتهای این مرحله منتقل کنید:

```sh
$ git chackout Stage3
```

#### امکان تنظیم پارامترهای پیکربندی
همانطور که دیدید اسکریپت پیکربندی به شکل پیشفرض آرگومانهای خط فرمان زیادی را می پذیرد. ما می توانیم در configure.ac ماکروهایی بنویسیم که موجب می شوند اسکریپت پیکربندی آرگومانهای غیر استانداردی را نیز بپذیرد. بعد در Makefile از مقدار این آرگومانها استفاده کنیم.

یکی از کاربردهای آرگومانهای خط فرمان، امکان کامپایل در دو حالت خطایابی Debugging یا انتشار Release است. کدی که در حالت خطایابی کامپایل می‌شود معمولا بهینه سازی نمی شود، و شامل همهٔ اطلاعات لازم برای خطایابی است. کامپایلرهای GCC اگر با پارامتر g- اجرا شوند این اطلاعات را به فایلهای باینری اضافه می کنند. در مقابل کدی که باید منتشر شود بهینه سازی می شود، و نباید شامل اطلاعات خطایابی باشد. برای بهینه سازی کد کامپایل شده با استفاده از کامپایلرهای GCC باید این کامپایلرها را با پارامتر `On-` اجرا کنیم. `n` می تواند یک عدد صحیح باشد. برای مثال `O2-`.

یک تفاوت دیگر نسخهٔ خطایابی با نسخهٔ انتشار، چاپ پیامهای Log توسط نسخهٔ خطایابی است. برنامه نویسان خطوطی را به کد برنامهٔ خود اضافه می کنند که این پیامها را چاپ کند. برای اینکه نسخهٔ انتشار شامل این خطوط اضافی نباشد، این خطوط برنامه را با یک ماکروی زبان C محافظت می کنند. برای مثال:

```C++
#ifdef DEBUG
std::cout << "Somthing about state of the program at this point\n";
#endif
```

بهترین جا برای تعریف این ماکروها فایل config.h است. برای تعریف یک ماکرو در config.h از ماکروی `AC_DEFINE` در configure.ac استفاده می‌کنیم. روش احضار این ماکرو به این شکل است:

```m4
AC_DEFINE(variable, value, [description])
```

نام ماکروی زبان C در پارامتر `variable` و مقدار آن در `value` نوشته می شود. اگر `description` ذکر شود، به عنوان توضیحات این ماکرو در فایل config.h.in و در نتیجه در config.h نوشته خواهد شد.

حال می خواهیم آپشن (option) خط فرمانی به اسکریپت پیکربندی اضافه کنیم که موجب شود برنامه در حالت انتشار کامپایل شود. اگر این آپشن ذکر نشود برنامه برای خطایابی کامپایل خواهد شد. طبق قاعده‌ٔ اسکریپتهای پیکربندی، چنین آپشنهایی به یکی از دو شکل زیر ذکر می‌شوند:

```text
--enable-feature[=arg]
--disable-feature
```

کاربر می تواند آرگومانی را برای این آپشن ذکر کند. اگر هیچ آرگومانی ذکر نشود، مقدار پیشفرض `yes` استفاده خواهد شد. ‍‍`enable-feature=no--` معادل `dieable-feature--` است.

برای افزودن یک آپشن خط فرمان دلخواه به اسکریپت پیکربندی، در configure.ac از ماکروی `AC_ARG_ENABLE` استفاده می کنیم. این ماکرو به شکل زیر تعریف شده است:

```m4
AC_ARG_ENABLE(feature, help-string, [action-if-given], [action-if-not-given])
```

عبارت `feature` نام یک قابلیت دلخواه است. برای کاربردی که ما در نظر داریم، از `release` به جای `feature` استفاده می‌کنیم. `help-string` یک متن راهنما است. اگر کاربر اسکریپت پیکربندی را با `help--` صدا بزند، این متن راهنما جلوی نام این آپشن چاپ خواهد شد. در انتهای این بخش روش صحیح ایجاد متون راهنما را توضیح خواهیم داد.

اگر در زمان اجرای اسکریپت پیکربندی، هر یک از دو آپشن خط فرمان ‍‍`enable-feature--` یا `dieable-feature--` ذکر شود، دستورهای پوستهٔ ‍`action-if-given` اجرا خواهند شد. مقدار آپشن یعنی ‍‍yes, no یا هرمقداری که کاربر در خط فرمان نوشته باشد در متغیر پوستهٔ ‍`enableval` در دسترس این دستورها خواهد بود. در عین حال این مقدار در متغیر `enable_feature` نیز در دسترس سایر بخشهای اسکریپت پیکربندی خواهد بود که بعد از این ماکرو نوشته شده اند. اگر این آپشن در خط فرمان نباشد، فرمانهای `action-if-not-given` اجرا خواهند شد.

برای ایجاد یک متن راهنمای زیبا و حرفه ای به شکل زیر از ماکروی ‍‍`AS_HELP_STRING` استفاده می کنیم:

```m4
AS_HELP_STRING(left-hand-side, right-hand-side, [indent-column = ‘26’], [wrap-column = ‘79’])
```

در این تعریف `left-hand-side` عنوانی است که سمت چپ خط راهنما نوشته می شود. معمولا نام آپشنها را این بخش می نویسیم. `right-hand-side` توضیحی است که سمت راست این عنوان دیده می شود.

با استفاده از دو پارامتر اختیاری `indent-column` و ‍‍`wrap-column` می توانید میزان تورفتگی و کل عرض قابل چاپ خطوط راهنما را تعیین کنید.

حال به کاربرد خاص خودمان باز میگردیم. بر اساس طرز کار ‍`AC_ARG_ENABLE` متوجه شدیم که بر اساس ذکر شدن یا نشدن آپشن release و مقدار احتمالی که کاربر تایپ می ‌کند، مقدار متغیر `enable_release` و کاری که باید انجام دهیم مطابق جدول زیر است:

{{< rawhtml >}}
<table border="1" cellpadding="2" style="width: 100%;direction:ltr;text-align: center;">
<thead><th style="text-align: center;">Giving option</th><th style="text-align: center;">Value of <i>enable_release</i></th><th style="text-align: center;">What should be done<th></thead>
<tr><td></td><td></td><td>Compile for debugging</td></tr>
<tr><td>--disable-release</td><td>no</td><td>Compile for debugging</td></tr>
<tr><td>--enable-release=no</td><td>no</td><td>Compile for debugging</td></tr>
<tr><td>--enable-release</td><td>yes</td><td>Compile for release</td></tr>
<tr><td>--enable-release=yes</td><td>yes</td><td>Compile for release</td></tr>
</table>
{{< /rawhtml >}}


برای مدیریت این حالات مختلف بهتر است از یک شرط پس از این ماکرو استفاده کنیم. در این شرط اگر مقدار متغیر `enable_release` برابر `yes` بود پارامترهای لازم را برای کامپایل در حالت انتشار به متغیر `CXXFLAGS` اضافه می کنیم، وگرنه پارامترهای لازم برای کامپایل در حالت خطایابی را در این متغیر می‌گذاریم. همچنین برای حالت خطایابی لازم است یک ماکروی زبان C به نام `DEBUG` با مقدار ۱ تعریف شود.

برای ساده کردن کار توسعه دهندگان و افزایش سازگاری اسکریپتهای پیکربندی با ماشینهای مقصد مختلف، بسته‌ٔ M4sh همراه با ماکروهایی عرضه می شود که به ساختارهای متداول اسکریپتهای پوسته بسط می یابند. این ماکروها در فایلهای configure.ac زیاد استفاده می شوند. یکی از این ماکروها مخصوص ساختارهای شرطی است، و به شکل زیر احضار می شود:

```m4
AS_IF(test1, [run-if-true1], [test2], [run-if-true2], ..., [run-if-false])
```

ابتدا test1 اجرا می شود. اگر نتیجهٔ آن درست بود، فهرست دستورهای run-if-true1 اجرا خواهند شد. وگرنه test2 در صورت وجود اجرا می شود؛ اگر مقدار آن درست بود دستورهای فهرست run-if-true2 اجرا خواهند شد. این روند تا انتها ادامه خواهد داشت. در نهایت اگر نتیجهٔ همهٔ تست ها غلط بود، دستورهای بخش اختیاری run-if-false اجرا خواهند شد.

حال از این اطلاعات استفاده می کنیم و تغییرات زیر را در configure.ac و Makefile.in وارد می‌کنیم:

**تغییرات configure.ac**

```m4
AC_ARG_ENABLE(release, AS_HELP_STRING([--enable-release],[Enables compiling for release or production (Default is no)]))

AS_IF([test "x$enable_release" = "xyes"],[CXXFLAGS="-O3"],
	[CXXFLAGS="-g"
	AC_DEFINE(DEBUG,1,[Enables printing log messages])])
```

**تغییرات Makefile.in**

```makefile
objects = stroke.o presenter.o sdlview.o main.o eventloop.o  

SOURCE = @SOURCE@

VPATH = $(SOURCE)

CPPFLAGS = @CPPFLAGS@ -I. -I$(SOURCE) -I$(SOURCE)/model -I$(SOURCE)/graphics

CXXFLAGS = @CXXFLAGS@

LIBS = @LIBS@

CXX = @CXX@

arty : $(objects)
	$(CXX) -o arty $(objects) $(LIBS)

eventloop.o : eventloop.cxx  eventloop.h  contract.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

sdlview.o : graphics/sdlview.cxx  graphics/sdlview.h  contract.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

main.o : main.cxx  presenter.h  graphics/sdlview.h  eventloop.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

stroke.o : model/stroke.cxx  model/stroke.h  model/line.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

presenter.o : presenter.cxx  presenter.h  contract.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -f arty $(objects)
	
.PHONY : clean
```

استفاده از ماکروی AC_DEFINE و سایر ماکروهایی که نمادهای زبان C را تعریف می کنند، روی خروجی autoheader تاثیر می‌گذارد. برای به روز گردانی config.h.in یک بار دستور `autoheader` را اجرا می کنیم. خطوط زیر به ابتدای config.h.in افزوده می شوند:

```C
/* Enables printing log messages */
#undef DEBUG
```

برای تولید اسکریپت پیکربندی جدید، یکبار دستور `autoconf` را اجرا می کنیم. حال می توانیم درستی کار خود را امتحان کنیم. ابتدا ببینیم راهنمایی که برای آپشن release به اسکریپت پیکربندی افزوده ایم چگونه نمایش داده می‌شود:

```text
$ ./configure --help
⁞
Optional Features:
  --disable-option-checking  ignore unrecognized --enable/--with options
  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
  --enable-release        Enables compiling for release or production (Default
                          is no)
⁞
```
  

اکنون یکبار اسکریپت پیکربندی را بدون آرگومان خط فرمان اجرا می‌کنیم، و بعد پروژه را کامپایل می‌کنیم:

```text
$ make clean
rm -f arty stroke.o presenter.o sdlview.o main.o eventloop.o  
$ ./configure
⁞
$ make
⁞
$ ls -l
total 672
-rwxrwxr-x. 1 babak babak 200944 Dec 13 16:23 arty
-rw-rw-r--. 1 babak babak   1698 Dec 13 16:21 config.h
-rw-rw-r--. 1 babak babak  17801 Dec 13 16:21 config.log
-rwxrwxr-x. 1 babak babak  26951 Dec 13 16:21 config.status
-rw-rw-r--. 1 babak babak  87904 Dec 13 16:23 eventloop.o
-rw-rw-r--. 1 babak babak  38592 Dec 13 16:23 main.o
-rw-rw-r--. 1 babak babak    864 Dec 13 16:21 Makefile
-rw-rw-r--. 1 babak babak 114120 Dec 13 16:23 presenter.o
-rw-rw-r--. 1 babak babak  76736 Dec 13 16:23 sdlview.o
-rw-rw-r--. 1 babak babak  98800 Dec 13 16:23 stroke.o
```

حال یک بار دیگر با آرگومان خط فرمان `enable-release--` پیکربندی و کامپایل می کنیم:


```text
$ make clean
rm -f arty stroke.o presenter.o sdlview.o main.o eventloop.o  
$ ./configure --enable-release
⁞
$ make
⁞
$ ls -l
total 124
-rwxrwxr-x. 1 babak babak 30792 Dec 13 16:27 arty
-rw-rw-r--. 1 babak babak  1701 Dec 13 16:27 config.h
-rw-rw-r--. 1 babak babak 17802 Dec 13 16:27 config.log
-rwxrwxr-x. 1 babak babak 26973 Dec 13 16:27 config.status
-rw-rw-r--. 1 babak babak  4384 Dec 13 16:27 eventloop.o
-rw-rw-r--. 1 babak babak  3024 Dec 13 16:27 main.o
-rw-rw-r--. 1 babak babak   865 Dec 13 16:27 Makefile
-rw-rw-r--. 1 babak babak  9104 Dec 13 16:27 presenter.o
-rw-rw-r--. 1 babak babak  7640 Dec 13 16:27 sdlview.o
-rw-rw-r--. 1 babak babak  3256 Dec 13 16:27 stroke.o
```

می بینید که حجم فایلهای باینری و اجرایی در حالت خطایابی بسیار بیشتر از حجم همان فایلها در حالت انتشار است. این موضوع نشان می دهد اطلاعات اضافی در فایلهای کامپایل شده در  حالت خطایابی قرار دارد یعنی تغییراتی که داده ایم به درستی کار می کنند.

به انتهای این مقاله رسیدیم، که اولین مقالهٔ آموزشی در مورد بستهٔ autotools بود. اکنون شما دیدگاه کلی نسبت به عملکرد اسکریپتهای پیکربندی و autoconf دارید، و با مراجعه به [راهنمای کامل autoconf](https://www.gnu.org/software/autoconf/manual/) می توانید برای هر نرم افزاری اسکریپت پیکربندی تولید کنید. برای بردن کد منبع به انتهای این بخش، می توانید در خط فرمان دستور زیر را اجرا کنید:

```sh
$ git checkout Stage4
```
دومین بخش این مقاله به نصب نرم افزار اختصاص دارد. در آنجا با تغییر دستی فایلهای configure.ac و Makefile.in امکان نصب نرم افزار را به بستهٔ نرم افزاری arty اضافه می کنیم.
در سومین مقالهٔ آموزش بستهٔ autotools در مورد automake و aclocal صحبت می کنیم. در آن مقاله میبینید که automake خودش فایلهای Makefile.in را تولید می‌کند، و امکانات نصب برنامه و مدیریت وابستگی ها را به شکل خودکار انجام می دهد.

