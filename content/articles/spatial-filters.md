---
title: "فیلترهای فضایی"
description: "فیلترهای فضایی روشی در پردازش سیگنال است که با استفاده از نمونه های محدود سیگنال دریافت شده در یک آرایهٔ حسگر از چند منبع در مکان نامعلوم، تخمین می زند هر منبع در چه زاویه ای قرار دارد"
tags: [فیلترهای فضایی,پردازش سیگنال,تخمین پارامتر,آرایهٔ حسگر]
categories: []
date: 2018-07-11T20:36:17+04:30
UsesMath: true
needsCommenting: true
---
فیلتر فضایی<sup>۳۳</sup> مفهومی از پردازش سیگنال است که پیش از ورود به عرصهٔ واسطهای مغز و رایانه توسط طراحان رادار، سونار و آرایه های آنتن و حسگر مورد استفاده قرار می‌گرفته است. سابقهٔ فیلترهای فضایی به جنگ جهانی دوم می رسد و نخستین روش محاسبهٔ فیلترهای فضایی به نام بارتلت<sup>۳۴</sup> ثبت شده است.

مسالهٔ بنیادین آرایهٔ حسگر که به فیلترهای فضایی منتهی می شود به این شکل است: فرض کنیم <span class="InlineFormula"><i>L</i></span> حسگر یا آنتن داریم که هر کدام در مکان <span class="InlineFormula"><b>r</b><sub>l</sub>,<i>l</i>=1,2,…,<i>L</i></span> قرار گرفته است. این حسگرها از <span class="InlineFormula"><i>M</i></span> منبع امواجی را دریافت می کنند و هر منبع امواج خود را در جهت زاویهٔ آزیموت <span class="InlineFormula">ɵ<sub>m</sub>,<i>m</i>=1,2,…,<i>M</i></span> می فرستد. حسگر <span class="InlineFormula"><i>l</i></span> در مجموع در هر لحظه از زمان سیگنال مختلط <span class="InlineFormula"><i>x</i><sub>l</sub>(<i>t</i>)</span> را در خروجی خود ایجاد می‌کند. می توانیم از این مقادیر یک بردار خروجی آرایه مثل <span class="InlineFormula"><b>x</b>(<i>t</i>) = [<i>x</i><sub>1</sub>(<i>t</i>),<i>x</i><sub>2</sub>(<i>t</i>),…<i>x</i><sub>2</sub>(<i>t</i>)]<sup>T</sup></span> تشکیل دهیم. هدف اصلی ما این است که با داشتن یک سری مشاهدات <span class="InlineFormula">{<b>x</b>(<i>t</i>)}</span> در زمانهای <span class="InlineFormula"><i>t</i>=1,2,…,<i>N</i></span> زوایای <span class="InlineFormula">ɵ<sub>m</sub>,<i>m</i>=1,2,…,<i>M</i></span> را تخمین بزنیم. به این ترتیب می توانیم بفهمیم هر منبع در کجا قرار دارد.

روشهای معروف تخمین برای این مساله در دو دستهٔ مبتنی بر طیف و پارامتری تقسیم می شوند. در همهٔ این روشها ماتریس کواریانس فضایی مورد استفاده قرار می گیرد که به شکل زیر تعریف می شود:

<table class="formula"><tr><td width="50%" class="InlineFormula"><b>R</b>=E[<b>x</b>(<i>t</i>)<b>x</b><sup>H</sup>(<i>t</i>)]</td><td>(۱)</td></tr></table>


در این عبارت <span class="InlineFormula"><b>x</b><sup>H</sup></span> ترانهادهٔ مزدوج یا *مزدوج هرمیتی*<sup>۱</sup> بردار <span class="InlineFormula"><b>x</b></span> است. در عمل تنها تخمین های مبتنی بر نمونه در دسترسند. تخمین نمونه ای ماتریس کواریانس عبارت است از:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><mstyle mathvariant="bold"><mrow><mover accent="true"><mi>R</mi><mo stretchy="false">̂</mo></mover></mrow></mstyle><mo stretchy="false">=</mo><mfrac><mn>1</mn><mi>N</mi></mfrac></mrow><mrow><munderover><mo stretchy="false">∑</mo><mrow><mi>t</mi><mo stretchy="false">=</mo><mn>1</mn></mrow><mi>N</mi></munderover><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle></mrow><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle></mrow><mi>H</mi></msup><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow></mrow><annotation encoding="StarMath 5.0">bold hat R = 1 over N sum from t=1 to N bold x(t){bold x}^H(t)</annotation></semantics></math></td><td>(۲)</td></tr></table>

روشهای تخمین پارامتر برای این مساله در دو دستهٔ رهیافتهای مبتنی بر طیف و رهیافتهای پارامتری قرار می گیرند. در روشهای دستهٔ اول تابعی طیف مانند از متغیرهای هدف مورد نظر یعنی زوایای آزیموت تشکیل میدهیم. مکان بلندترین قله های این تابع به عنوان تخمین زوایای مورد نظر به کار می روند. در روش پارامتری لازم است روی همهٔ پارامترهای مورد نظر همزمان جستجو انجام شود. رهیافت دوم اگرچه به قیمت افزایش پیچیدگی محاسباتی، تخمین های دقیقتری ارائه می‌دهد.

## روشهای تخمین مبتنی بر طیف
روشهای مبتنی بر طیف که در این بخش توصیف می‌شوند به دو گروه روشهای پرتودیسی<sup>۲</sup> و روشهای مبتنی بر زیرفضا تقسیم می شوند.

### روشهای مبنی بر پرتودیسی
نخستین تلاشهای تعیین مکان خودکار منابع سیگنال به وسیلهٔ آرایهٔ آنتن با روش پرتودیسی انجام گرفته است. ایدهٔ کلی این است که آنتنها را در هر لحظه به یک جهت هدایت کنیم و توان خروجی را اندازه بگیریم. جهت هدایتی که بیشترین توان را ایجاد می کند تخمین زوایای آزیموت را به دست می دهد. پاسخ جمعی آرایه با ایجاد یک ترکیب خطی از پاسخ حسگرها هدایت می شود:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mi>y</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow><mo stretchy="false">=</mo><mrow><munderover><mo stretchy="false">∑</mo><mrow><mi>l</mi><mo stretchy="false">=</mo><mn>1</mn></mrow><mi>L</mi></munderover><msubsup><mi>w</mi><mi>l</mi><mtext>*</mtext></msubsup></mrow></mrow><msub><mi>x</mi><mi>l</mi></msub><mrow><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow><mo stretchy="false">=</mo><msup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>H</mi></msup></mrow><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow></mrow><annotation encoding="StarMath 5.0">y(t)=sum from l=1 to L w^&quot;*&quot;_l x_l(t)={bold w}^H bold{x}(t)</annotation></semantics></math></td><td>(۳)</td></tr></table>

با وجود *N* نمونهٔ <span class="InlineFormula"><i>y</i>(1),<i>y</i>(2),…,<i>y</i>(<i>N</i>)</span> توان خروجی به این شکل حساب می شود:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mi>P</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mo stretchy="false">)</mo></mrow><mo stretchy="false">=</mo><mfrac><mn>1</mn><mi>N</mi></mfrac></mrow><mrow><mrow><munderover><mo stretchy="false">∑</mo><mrow><mi>t</mi><mo stretchy="false">=</mo><mn>1</mn></mrow><mi>N</mi></munderover><msup><mrow><mfenced open="∣" close="∣"><mrow><mi>y</mi><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow></mrow></mfenced></mrow><mn>2</mn></msup></mrow><mo stretchy="false">=</mo><mfrac><mn>1</mn><mi>N</mi></mfrac></mrow><mrow><munderover><mo stretchy="false">∑</mo><mrow><mi>t</mi><mo stretchy="false">=</mo><mn>1</mn></mrow><mi>N</mi></munderover><msup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>H</mi></msup></mrow><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle></mrow><mi>H</mi></msup><mrow><mo stretchy="false">(</mo><mrow><mi>t</mi></mrow><mo stretchy="false">)</mo></mrow><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle><mo stretchy="false">=</mo><msup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>H</mi></msup></mrow><mover accent="true"><mrow><mstyle mathvariant="bold"><mrow><mi>R</mi></mrow></mstyle></mrow><mo stretchy="false">^</mo></mover><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><annotation encoding="StarMath 5.0">P(bold w)=1 over N sum from t=1 to N {abs{y(t)}}^2 = 1 over N sum from t=1 to N {bold w}^H bold x(t){bold x}^H(t) bold w={bold w}^H hat{bold R} bold w</annotation></semantics></math></td><td>(۴)</td></tr></table>

که در آن <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mover accent="true"><mrow><mstyle mathvariant="bold"><mrow><mi>R</mi></mrow></mstyle></mrow><mo stretchy="false">̂</mo></mover></mrow><annotation encoding="StarMath 5.0">hat{bold R}</annotation></semantics></math> تخمین ماتریس کواریانس فضایی است. رهیافتهای مختلف پرتودیسی با انتخابهای مختلف بردار وزن <span class="InlineFormula"><b>w</b></span> متناظر است.

<!--
#### پرتودیسی سنتی
پرتودیسی سنتی یا بارتلت توسعهٔ طبیعی تحلیل طیفی کلاسیک فوریه به دادهٔ آرایهٔ حسگر است. این الگوریتم به ازای هندسهٔ دلخواه آرایهٔ حسگر، توان خروجی پرتودیسی را برای یک سیگنال ورودی بیشینه می سازد. فرض کنید میخواهیم توان خروجی را برای یک جهت معین <span class="InlineFormula">ɵ</span> بیشینه کنیم. باوجود سیگنالی که از جهت <span class="InlineFormula">ɵ</span> می‌آید، خروجی آرایه تحت اثر نویز افزایشی تخریب شده و به شکل زیر نوشته می شود:

<table class="formula"><tr><td width="50%" class="InlineFormula"><b>x</b>(<i>t</i>)=<b>a</b>(ɵ)<i>s</i>(<i>t</i>)+<b>n</b>(<i>t</i>)</td><td>(۵)</td></tr></table>

در این معادله <span class="InlineFormula"><b>a</b>(ɵ)</span> به هندسهٔ آرایهٔ حسگرها، زاویهٔ آزیموت، بسامد موج و پاسخ فرکانسی حسگرها بستگی دارد و بردار هدایت<sup>۳</sup> آرایه نامیده می‌شود (برای توضیحات بیشتر ر.ک. مرجع ۱). <span class="InlineFormula"><i>s</i>(<i>t</i>)</span> نیز مشخصهٔ منبع موج است که ممکن است به آهستگی با زمان تغییر کند. <span class="InlineFormula"><b>n</b>(t)</span> بردار نویز است و هر مولفه اش یک متغیر تصادفی با واریانس مشترک <span class="InlineFormula">σ<sup>2</sup></span> است. تحت فرض سفید بودن نویز، میانگین هر مولفه نیز صفر است. برای به دست آوردن یک پاسخ غیر بدیهی، این محدودیت را روی بردار وزن می‌گذاریم که اندازهٔ آن همواره برابر یک باشد. در این صورت راه حل بیشینه سازی عبارت است از:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><msub><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi mathvariant="italic">BF</mi></msub><mo stretchy="false">=</mo><mfrac><mrow><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow></mrow><mrow><msqrt><mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle></mrow><mi>H</mi></msup><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow></mrow></msqrt></mrow></mfrac></mrow></mrow><annotation encoding="StarMath 5.0">{bold w}_BF={bold a(%theta)}over{sqrt {{bold a}^H(%theta) bold a(%theta)}}</annotation></semantics></math></td><td>(۶)</td></tr></table>

بردار وزن فوق را میتوان یک فیلتر فضایی در نظر گرفت که بر سیگنال ورودی منطبق شده است. به شکل شهودی وزن دهی آرایه ای تاخیرها و تضعیفهای ممکن سیگنال روی حسگرهای مختلف را متعادل می‌کند تا مشارکت هر کدام را در خروجی نهایی به شکل بیشینه ترکیب کند.

با جاگذاری بردار وزن رابطهٔ (۶) در معادلهٔ (۴) طیف فضایی کلاسیک به دست می آید:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>P</mi><mi mathvariant="italic">BF</mi></msub><mrow><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow><mo stretchy="false">=</mo><mfrac><mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle></mrow><mi>H</mi></msup><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow><mover accent="true"><mstyle mathvariant="bold"><mrow><mi>R</mi></mrow></mstyle><mo stretchy="false">̂</mo></mover><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow></mrow><mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle></mrow><mi>H</mi></msup><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow><mstyle mathvariant="bold"><mrow><mi>a</mi></mrow></mstyle><mrow><mo stretchy="false">(</mo><mrow><mo stretchy="false">θ</mo></mrow><mo stretchy="false">)</mo></mrow></mrow></mfrac></mrow></mrow><annotation encoding="StarMath 5.0">P_BF(%theta)={{bold a}^H(%theta) hat bold R bold a(%theta)}over{{bold a}^H(%theta)bold a(%theta)}</annotation></semantics></math></td><td>(۷)</td></tr></table>

برای یافتن زوایای <span class="InlineFormula">ɵ</span> که طیف فضایی را بیشینه می‌کنند از روشهای محاسبات عددی استفاده می‌کنیم.
-->

## استفاده از فیلترهای فضایی در طراحی واسطهای مغز و رایانه

همانطور که در [مقالهٔ واسط مغز و رایانه](../bci/index.html) گفتیم بعضی انواع واسطهای مغز و ماشین برای تشخیص فیزیولوژی آسیب عصبی مغز و تشخیص مکان کانون صرع به کار می‌روند. از برخی انواع آن نیز برای درمان عوارض بیماری پارکینسون یا درمان اختلالات شدید روانی استفاده می شود. نوع سوم که بیشتر مورد توجه ما است با هدف توانبخشی طراحی می شود و کانالهای ارتباطی و کنترلی در اختیار کاربر قرار می دهد که به مسیرهای ارتباطی متداول اعصاب جانبی و ماهیچه ها بستگی ندارد. به طور خلاصه این واسطها حالت ذهنی کاربر را رمزگشایی کرده و به فرامین کنترلی تبدیل می‌کنند. همچنین در آن مقاله دلایلی مبنی بر اهمیت تفکیک منابع فعالیت الکتریکی ذکر کردیم. تفکیک منابع توان تفکیک کنندگی رده بند را افزایش می‌دهد، به علاوه آگاهی از همبستگی منابع فعالیت الکتریکی با ساختار و آناتومی مغز برای عصب شناسان اهمیت زیادی دارد.

به گفتهٔ بلانکرتز در مرجع [۲]:

> پتانسیلهای نوار مغزی که در سطح جمجمه دریافت می‌شوند به دلیل رسانش حجمی دقت تفکیک فضایی ضعیفی دارند. در یک پژوهش شبیه سازی (مرجع [۳])، مشخص شده منابع موجود در سه سانتیمتری هر الکترود تنها در نصف پتانسیل آن الکترود مشارکت دارند. این موضوع مخصوصاً وقتی مساله ساز است که سیگنال مطلوب مثل ریتم باریکهٔ حسی حرکتی ضعیف باشد درحالی که منابع دیگر مثل ریتم آلفای قشر بینایی یا آرتیفکت های حرکتی و عضلانی در همان باند بسامدی سیگنال قویتری تولید می‌کنند.

به این دلایل من در پروژهٔ پایان نامهٔ خود بخشی را به فیلترهای فضایی اختصاص داده ام. قصد دارم در قالب چند مقاله تعدادی از الگوریتم های محاسب فیلترهای فضایی را معرفی کنم. اینها شامل الگوهای فضایی مشترک (CSP) و مشتقات آن، جداسازی کورکورانهٔ منبع (BSS) و هم-مدولاسیون توان منبع (SPoC) است. یک مقاله در مورد شکل ساده و بنیادی CSP نوشته شده ولی هنوز کار زیادی دارد و به همین دلیل در وضعیت پیشنویس است.


***
## پانویسها

1. Hermitian conjugate
2. Beamforming
3. Steering vector


## مراجع
1. Krim, Hamid, (1996), Two decades of array signal processing research, IEEE Signal Processing Magazine, July 1996
2. Blankertz, Benjamin, (2008), Optimizing Spatial Filters for Robust EEG Single-Trial Analysis, IEEE Signal Processing Magazine, VOL. XX, 2008
3. Nunez, Paul L. (1997), EEG coherency I: statistics, reference electrode, volume conduction, Laplacians, cortical imaging, and interpretation at multiple scales. Electroencephalogr. Clin. Neurophysiol., 103:499–515, 1997

