---
title: "الگوریتم CSP"
description: "الگوریتم CSP یا الگوهای فضایی مشترک یکی از روشهای محاسبهٔ فیلترهای فضایی است که بیشتر برای طراحی واسط مغز و رایانه به کار می رود"
tags: [الگوریتم CSP,فیلترهای فضایی,واسطهای مغز و رایانه,پردازش سیگنال]
categories: []
date: 2018-07-27T23:08:32+04:30
draft: true
containsCode: true
UsesMath: true
needsCommenting: true
---
در این مقاله قصد داریم الگوریتم الگوهای فضایی مشترک<sup>۱</sup> یا CSP را معرفی کنیم. این الگوریتم برای محاسبهٔ فیلترهای فضایی برای تشخیص اثرات همگامی و یا ناهمگامی مرتبط با رویداد (ERD یا ERS) و طراحی واسطهای مغز و رایانه بر اساس این پدیده ها بسیار سودمند است. با داشتن دو کلاس از بردارهای اندازه گیری در فضای کثیرالبعد سیگنال، الگوریتم CSP فیلترهای فضایی را می‌یابد که واریانس سیگنال های یک کلاس را بیشینه می کنند و همزمان واریانس نمونه های کلاس دیگر را کمینه می سازند. پس از اینکه سیگنال نوار مغزی تحت صافی فرکانس گزین با بسامد ریتمهای مطلوب قرار گرفت، واریانس زیاد معادل ریتم قوی و واریانس کم معادل ریتم ضعیف یا تضعیف شده است. از طرف دیگر واریانس سیگنالی که تحت صافی فرکانس گزین قرار گرفته با توان باند سیگنال اولیه در همان بازهٔ فرکانسی مساوی است. در نتیجه با استفاده از CSP می توانیم به شکل موثری میان حالات ذهنی که با پدیده های ERD/ERS مشخص می‌شوند تمایز ایجاد کنیم.

## مراحل الگوریتم CSP
فرض کنیم ماتریس <span class="InlineFormula"><b>X</b>∈ℝ<sup><i>C</i>×<i>T</i></sup></span> بخش کوتاهی از سیگنال نوار مغزی و نظیر یک آزمایش<sup>۲</sup> باشد. <span class="InlineFormula"><i>C</i></span> تعداد کانالهای ضبط و <span class="InlineFormula"><i>T</i></span> تعداد نقاط نمونه برداری است. همچنین فرض کنیم سیگنال نمونهٔ <span class="InlineFormula"><b>X</b></span> متعلق به کلاس (+) (یا کلاس (-)) باشد. هدف یافتن فیلترهای فضایی <span class="InlineFormula"><b>w</b><sub><i>j</i></sub></span> برای <span class="InlineFormula"><i>j</i>=1,2,…,<i>J</i></span> است طوری که <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mi mathvariant="italic">var</mi><mrow><mo stretchy="false">[</mo><mrow><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi><mi>T</mi></msubsup><msub><mrow><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle></mrow><mi>t</mi></msub></mrow><mo stretchy="false">]</mo></mrow></mrow><annotation encoding="StarMath 5.0">var[{bold w}_j^T {bold x}_t]</annotation></semantics></math> یا به طور معادل <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi><mi>T</mi></msubsup><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle><msup><mrow><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle></mrow><mi>T</mi></msup><msub><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi></msub></mrow><annotation encoding="StarMath 5.0">{bold w}_j^T bold X {bold X}^T {bold w}_j</annotation></semantics></math> بیشینه (یا کمینه) باشد.

برای یافتن فیلترهای <span class="InlineFormula"><b>w</b><sub><i>j</i></sub></span> فرض کنیم سیگنال تحت صافی میان گذر قرار گرفته و مرکزیت یافته و مقیاس دهی شده است. <span class="InlineFormula"><b>Σ</b><sup>(+)</sup>∈ℝ<sup><i>C</i>×<i>C</i></sup></span> و <span class="InlineFormula"><b>Σ</b><sup>(-)</sup>∈ℝ<sup><i>C</i>×<i>C</i></sup></span> تخمین ماتریس های کواریانس نمونه های سیگنال در کلاس های (+) و (-) هستند. به شکل زیر:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mo stretchy="false">Σ</mo></mrow></mstyle></mrow><mrow><mrow><mo stretchy="false">(</mo><mrow><mi>c</mi></mrow><mo stretchy="false">)</mo></mrow></mrow></msup><mo stretchy="false">=</mo><mfrac><mn>1</mn><mrow><mfenced open="∣" close="∣"><msub><mi>I</mi><mi>c</mi></msub></mfenced></mrow></mfrac></mrow><mrow><munder><mo stretchy="false">∑</mo><mrow><mrow><mi>i</mi><mo stretchy="false">∈</mo><msub><mi>I</mi><mi>c</mi></msub></mrow></mrow></munder><msub><mrow><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle></mrow><mi>i</mi></msub></mrow><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle></mrow><mi>i</mi><mi>T</mi></msubsup><mi/><mspace width="2em"/><mi>,</mi><mrow><mi>c</mi><mo stretchy="false">∈</mo><mfenced open="{" close="}"><mrow><mtext>+</mtext><mi>,</mi><mtext>-</mtext></mrow></mfenced></mrow></mrow><annotation encoding="StarMath 5.0">{bold %SIGMA}^{(c)} = 1 over {abs I_c} sum csub {i in I_c} {bold X}_i{bold X}_i^T~~,c in  left lbrace &quot;+&quot;,&quot;-&quot; right rbrace</annotation></semantics></math></td><td>(۱)</td></tr></table>

در این عبارت <span class="InlineFormula"><i>I</i><sub><i>c</i></sub></span> مجموعهٔ اندیس های نمونه های کلاس <span class="InlineFormula"><i>c</i></span> و <span class="InlineFormula">|<i>I<sub>c</sub></i>|</span> تعداد این اندیس‌ها است. اگر <span class="InlineFormula"><b>W</b></span> ماتریس همهٔ بردارهای <span class="InlineFormula"><b>w</b><sub><i>j</i></sub></span> باشد (هر ستون آن یک فیلتر فضایی است) آنگاه این فیلترها با قطری سازی همزمان تخمین ماتریس‌ های کواریانس یافت می شوند:

<table class="formula"><tr><td width="50%"><span class="InlineFormula"><b>W</b><sup>T</sup><b>Σ</b><sup>(+)</sup><b>W</b>=<b>Λ</b><sup>(+)</sup></span><br/><span class="InlineFormula"><b>W</b><sup>T</sup><b>Σ</b><sup>(-)</sup><b>W</b>=<b>Λ</b><sup>(-)</sup></span></td><td>(۲)</td></tr></table>

ماتریس <span class="InlineFormula"><b>W</b></span> طوری مقیاس می‌شود که <span class="InlineFormula"><b>Λ</b><sup>(+)</sup>+<b>Λ</b><sup>(-)</sup>=<b><i>I</i></b></span>. این کار خیلی ساده با حل مسالهٔ مقدار ویژهٔ تعمیم یافتهٔ زیر انجام می‌شود:


<table class="formula"><tr><td width="50%"><span class="InlineFormula"><b>Σ</b><sup>(+)</sup><b>w</b>=λ<b>Σ</b><sup>(-)</sup><b>w</b></span></td><td>(۳)</td></tr></table>

در اکتاو یا متلب می‌توانید این مساله را با اجرای دستور زیر حل کنید:
```m
[W,lambda] = eig(S1,S1+S2);
```

اگر ستون های ماتریس <span class="InlineFormula"><b>W</b></span> را بردارهای ویژهٔ رابطهٔ (۳) تشکیل دهد و <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><msubsup><mo stretchy="false">λ</mo><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mi>c</mi></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup><mo stretchy="false">=</mo><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi><mi>T</mi></msubsup></mrow><msup><mrow><mstyle mathvariant="bold"><mrow><mo stretchy="false">Σ</mo></mrow></mstyle></mrow><mrow><mrow><mo stretchy="false">(</mo><mrow><mi>c</mi></mrow><mo stretchy="false">)</mo></mrow></mrow></msup><msub><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi></msub></mrow><annotation encoding="StarMath 5.0">%lambda_j^{(c)}={bold w}_j^T{bold %SIGMA}^{(c)}{bold w}_j</annotation></semantics></math> عناصر قطری نظیر ماتریس <span class="InlineFormula"><b>Λ</b><sup>(<i>c</i>)</sup></span> و به گونه ای باشد که <span class="InlineFormula">λ</span> در عبارت (۷) مساوی <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><msubsup><mo stretchy="false">λ</mo><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mtext>+</mtext></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup><mo stretchy="false">/</mo><msubsup><mo stretchy="false">λ</mo><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mtext>-</mtext></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup></mrow></mrow><annotation encoding="StarMath 5.0">%lambda_j^{(&quot;+&quot;)}/%lambda_j^{(&quot;-&quot;)}</annotation></semantics></math> باشد آنگاه رابطهٔ (۲) برقرار می شود. نکتهٔ دیگر آنکه <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><msubsup><mo stretchy="false">λ</mo><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mi>c</mi></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup><mo stretchy="false">≥</mo><mn>0</mn></mrow></mrow><annotation encoding="StarMath 5.0">%lambda_j^{(c)}&gt;=0</annotation></semantics></math> واریانس سیگنالهای جانشین (نگاشت شده) در کلاس <span class="InlineFormula">c</span> است و <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mrow><msubsup><mo stretchy="false">λ</mo><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mtext>+</mtext></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup><mo stretchy="false">+</mo><msubsup><mo stretchy="false">λ</mo><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mtext>-</mtext></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup></mrow><mo stretchy="false">=</mo><mn>1</mn></mrow><annotation encoding="StarMath 5.0">%lambda_j^{(&quot;+&quot;)}+%lambda_j^{(&quot;-&quot;)}=1</annotation></semantics></math>. درنتیجه یک مقدار <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msubsup><mi>λ</mi><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mtext>+</mtext></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup></mrow><annotation encoding="StarMath 5.0">%lambda_j^{(&quot;+&quot;)}</annotation></semantics></math> (یا <math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msubsup><mi>λ</mi><mi>j</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mtext>-</mtext></mrow><mo stretchy="false">)</mo></mrow></mrow></msubsup></mrow><annotation encoding="StarMath 5.0">%lambda_j^{(&quot;-&quot;)}</annotation></semantics></math>) نزدیک به یک نشان می دهد فیلتر فضایی نظیر <span class="InlineFormula"><b>w</b><sub><i>j</i></sub></span> در شرایط کلاس مثبت (منفی) واریانس زیاد و در شرایط منفی (مثبت) واریانس کمی تولید می‌کند. این تقابل میان دو کلاس تمایز را آسان می کند.

برای دسته بندی ابتدا فیلترهای فضایی از روی مجموعه نمونه های آموزشی یعنی سیگنالهایی که دسته یا حالت ذهنی آنها معین است تعیین می شوند. سپس از لگاریتم توان سیگنال نگاشت شده به عنوان ویژگی استخراج شده در تحلیل ممیزهٔ خطی با معیار فیشر استفاده می کنیم تا نمونه های جدید سیگنال را دسته بندی کنیم. تابع ممیزهٔ این تحلیل به شکل زیر است:

<table class="formula"><tr><td width="50%"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><mi>f</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle><mo>;</mo><mfenced open="{" close="}"><mrow><msub><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi></msub></mrow></mfenced><mi>,</mi><mfenced open="{" close="}"><mrow><msub><mi>β</mi><mi>j</mi></msub></mrow></mfenced></mrow><mo stretchy="false">)</mo></mrow><mo stretchy="false">=</mo><mrow><munderover><mo stretchy="false">∑</mo><mrow><mi>j</mi><mo stretchy="false">=</mo><mn>1</mn></mrow><mi>J</mi></munderover><msub><mi>β</mi><mi>j</mi></msub></mrow></mrow><mi>log</mi><mrow><mrow><mo stretchy="false">(</mo><mrow><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi><mi>T</mi></msubsup><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle><msup><mrow><mstyle mathvariant="bold"><mrow><mi>X</mi></mrow></mstyle></mrow><mi>T</mi></msup><msub><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mi>j</mi></msub></mrow><mo stretchy="false">)</mo></mrow><mo stretchy="false">+</mo><msub><mi>β</mi><mn>0</mn></msub></mrow></mrow><annotation encoding="StarMath 5.0">f(bold X;left lbrace {bold w}_j right rbrace,left lbrace %beta_j right rbrace)=sum from j=1 to J %beta_j log({bold w}_j^T bold X {bold X}^T {bold w}_j)+%beta_0</annotation></semantics></math></td><td>(۸)</td></tr></table>

ضرایب <span class="InlineFormula">β<sub><i>j</i></sub>&nbsp;&nbsp;<i>j</i>=1,2,…,<i>J</i></span> و بایاس <span class="InlineFormula">β<sub>0</sub></span> با تحلیل ممیزهٔ خطی محاسبه می شود. به دلیل خواص فیلترهای فضایی متداول است که از بردارهای ویژهٔ نظیر دو انتهای طیف مقادیر ویژه به عنوان فیلتر فضایی استفاده کنیم. اگر تعداد فیلترهای منتخب یا مقدار <span class="InlineFormula"><i>J</i></span> خیلی کوچک باشد ممکن است رده بند نتواند کاملاً میان دو کلاس تمایز ایجاد کند. از طرف دیگر اگر <span class="InlineFormula"><i>J</i></span> خیلی بزرگ باشد ممکن است ضرایب و بایاس <span class="InlineFormula">β<sub>j</sub></span> اورفیت<sup>۳</sup> شود. می توان تعداد بهینهٔ فیلترهای انتخاب شده را با ارزیابی روی دادهٔ آموزشی تعیین نمود. در مرجع [۲] از تعداد ثابت ۶ فیلتر فضایی، یعنی ۳ فیلتر از هر انتهای طیف مقادیر ویژه استفاده کرده اند.

## نمونه‌ٔ اجرای CSP
برای این که ببینیم CSP چطور عمل می کند این الگوریتم را روی یک مجموعه داده آزمایش می‌کنیم. این مجموعه شامل دادهٔ ساختگی بسیار ساده ای است. دو کلاس داریم که هر کدام شامل تعدادی الگوی دو بعدی با توزیع نرمال است. این دو کلاس به شکل خطی جداپذیر نیستند. بر اساس روش تولید این نمونه ها از روی ماتریس های کواریانس، انتظار داریم دو خوشهٔ تقریباً بیضوی را تشکیل دهند. شکل (۱) این دو کلاس را نمایش می دهد.

<figure><img src="../../images/csp-demo1-1.png" style="width: 500px;" alt="دو کلاس با توزیع نرمال که خطی جداپذیر نیستند"><figcaption style="text-align: center;">شکل ۱: دو کلاس با توزیع نرمال که خطی جداپذیر نیستند</figcaption></figure>

دو فیلتر فضایی <span class="InlineFormula"><b>w</b><sub>1</sub></span> و <span class="InlineFormula"><b>w</b><sub>2</sub></span> نمونه های دو کلاس را به شکل خطی نگاشت می کنند. این نگاشت طوری است که نمونه های نگاشت شدهٔ کلاس ‍۱ بیشترین واریانس را در راستای <math xmlns="http://www.w3.org/1998/Math/MathML" alttext="transposed w 1 times x"><semantics><mrow><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mn>1</mn><mi>T</mi></msubsup><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle></mrow><annotation encoding="StarMath 5.0">{bold w}_1^T bold x</annotation></semantics></math> و کمترین واریانس را در راستای <math xmlns="http://www.w3.org/1998/Math/MathML" alttext="transposed w 2 times x"><semantics><mrow><msubsup><mrow><mstyle mathvariant="bold"><mrow><mi>w</mi></mrow></mstyle></mrow><mn>2</mn><mi>T</mi></msubsup><mstyle mathvariant="bold"><mrow><mi>x</mi></mrow></mstyle></mrow><annotation encoding="StarMath 5.0">{bold w}_2^T bold x</annotation></semantics></math> داشته باشد. عکس این موضوع در مورد نمونه های کلاس ۲ برقرار است. شکل (۲) نمونه های نگاشت شدهٔ هر دو کلاس را نشان می دهد.

<figure><img src="../../images/csp-demo1-2.png" style="width: 500px;" alt="نمونه های نگاشت شدهٔ دو کلاس با فیلترهای فضایی حاصل از CSP"><figcaption style="text-align: center;">شکل ۲: نمونه های نگاشت شدهٔ دو کلاس با فیلترهای فضایی حاصل از CSP</figcaption></figure>

این دو کلاس همچنان جداپذیر خطی نیستند ولی با تبدیل لگاریتم توان باند تا حد زیادی به شکل خطی جداپذیر می‌شوند. یک تحلیل ممیزهٔ خطی می تواند مرز تصمیم بهینه ای میان نمونه های این دو کلاس ایجاد کند. شکل ۳ نمونه های لگاریتم توان باند را به همراه مرز تصمیم مربوطه نشان می دهد.

<figure><img src="../../images/csp-demo1-3.png" style="width: 500px;" alt="لگاریتم توان باند نمونه های دو کلاس پس از اعمال فیلترهای فصایی"><figcaption style="text-align: center;">شکل ۳: لگاریتم توان باند نمونه های دو کلاس پس از اعمال فیلترهای فصایی</figcaption></figure>

حال بیایید از این رده بند برای برچسب گذاری نمونه های از پیش دیده نشده استفاده کنیم. نتیجه ای که به دست می آید جالب و آموزنده است. تعمیم رده بند بر نقاط جدید و از پیش دیده نشده موجب ایجاد نواحی تصمیم غیر بیضوی می‌شود. این نتیجه در شکل ۴ مشخص است.

<figure><img src="../../images/csp-demo1-4.png" style="width: 500px;" alt=" تعمیم رده بند خطی با فیلترهای فضایی CSP بر نقاط جدید از کل فضا"><figcaption style="text-align: center;">شکل ۴: تعمیم رده بند خطی با فیلترهای فضایی CSP بر نقاطی از کل فضا</figcaption></figure>

کد اکتاو (یا متلب) که برای تولید این داده ها و رسم نمودارها به کار رفته در ادامه دیده می‌شود:
```m
function cspdemo
	
	Up = [-0.93000   0.34000; 0.34000   0.93000];
	Lp = [0.250000   0.00000; 0.00000   2.50000];
	Um = [-1, 1; 1 1];
	Lm = [0.25000   0.00000; 0.00000   1.50000];
	
	% Generate samples randomly
	Xp = Up*Lp*randn(2,50);
	Xm = Um*Lm*randn(2,50);
	
	% Estimates of covariance matrices
	Sp=zeros(2,2);
	for k=1:50
		Sp=Sp+Xp(:,k)*Xp(:,k)';
	end
	Sp=Sp/50;
	
	Sm=zeros(2,2);
	for k=1:50
		Sm=Sm+Xm(:,k)*Xm(:,k)';
	end
	Sm=Sm/50;
	
	% Calculate spatial filters
	[W,Lambda] = eig(Sp,Sp+Sm);
	
	% Map points by spatial fiters
	Yp = W'*Xp;
	Ym = W'*Xm;
	
	% Map to the Phi space (log-power)
	phi_p = log(Yp.^2);
	phi_m = log(Ym.^2);
	
	% Perform Fisehr LDA to obtain a discriminant function
	[beta,beta0] = FisherLDA(phi_p, phi_m);
	
	% Plots
	figure
	plot(Xp(1,:),Xp(2,:),'r+');
	hold on
	plot(Xm(1,:),Xm(2,:),'bo');
	title('Raw data')
	xlabel('x_1')
	ylabel('x_2')
	
	figure
	plot(Yp(1,:),Yp(2,:),'r+');
	hold on
	plot(Ym(1,:),Ym(2,:),'bo');
	title('After CSP filtering')
	xlabel('w_1^T X')
	ylabel('w_2^T X')
	
	figure
	plot(phi_p(1,:),phi_p(2,:),'r+');
	hold on
	plot(phi_m(1,:),phi_m(2,:),'bo');
	% Decision boundary
	if abs(beta(2))>abs(beta(1))
		phi1l = min([phi_p(1,:), phi_m(1,:)]);
		phi1h = max([phi_p(1,:), phi_m(1,:)]);
		phi2l = (-beta0-beta(1)*phi1l)/beta(2);
		phi2h = (-beta0-beta(1)*phi1h)/beta(2);
	else
		phi2l = min([phi_p(2,:), phi_m(2,:)]);
		phi2h = max([phi_p(2,:), phi_m(2,:)]);
		phi1l = (-beta0-beta(2)*phi2l)/beta(1);
		phi1h = (-beta0-beta(2)*phi2h)/beta(1);
	end
	line([phi1l, phi1h],[phi2l, phi2h])
	title('Phi space (log-power)')
	xlabel('log(w_1^T X X^T w_1)')
	ylabel('log(w_2^T X X^T w_2)')
	
	% Generating some test points and classifying them
	Xtest = randn(2,1000)*2;
	Y = W'*Xtest;
	phi = log(Y.^2);
	Ip = beta(1)*phi(1,:) + beta(2)*phi(2,:) + beta0 > 0;
	figure
	plot(Xtest(1,Ip), Xtest(2,Ip), 'r+');
	hold on
	plot(Xtest(1,~Ip), Xtest(2,~Ip), 'bo');
	title('Test samples')
	xlabel('x_1')
	ylabel('x_2')
end
	
function [w,w0] = FisherLDA(C1,C2)
	% C1 and C2 : two matrices with p rows for dimensions and n_i columns for samples
	p = size(C1,1);
	n1 = size(C1,2);
	n2 = size(C2,2);
	S1 = zeros(p,p);
	S2 = zeros(p,p);
	% Covariance matrices
	for t=1:n1
		S1 = S1+C1(:,t)*C1(:,t)';
	end
	
	for t=1:n2
		S2 = S2+C2(:,t)*C2(:,t)';
	end
	
	% the pooled within-class sample covariance matrix
	Sw = (S1+S2)/(n1+n2-2);
	
	% Averages
	m1 = mean(C1,2);
	m2 = mean(C2,2);
	
	% Results
	w = inv(Sw)*(m1-m2);
	w0 = -0.5*(m1+m2)'*inv(Sw)*(m1-m2)-log(n2/n1);
	
end
```
## تعمیم الگوریتم CSP به حالتی که چند کلاس سیگنال داریم
آنچه در قسمتهای قبل گفته شد برای حالت دو کلاسه است. در برخی از کاربردها لازم است بیش از دو حالت ذهنی رده بندی شود. آنطور که دورنهژ و بلانکرتز در منبع [۶] گفته اند افزایش تعداد کلاسها می تواند نرخ انتقال اطلاعات واسط مغز و ماشین را افزایش دهد به شرطی که خطای دسته بندی به حد کافی کم باشد.

برای تعمیم CSP به حالت چند کلاسه سه راه مختلف وجود دارد.

### ره اول: رده بندی دو به دو و رای گیری
در این روش برای هر ترکیبی از جفت کلاسها یک مجموعه فیلتر فضایی با CSP دو کلاسه محاسبه می شود. هنگام رده بندی یک سیگنال جدید، به روش قبل برای هر جفت کلاس رده بندی می شود و درنهایت به کلاسی منسوب می شود که بیشترین رای را آورده باشد (بیشتر رده بندها آن کلاس را انتخاب کرده باشند)

### راه دوم: یکی در مقابل همه
در این روش در زمان آموزش سیگنالهای هر کلاس را در مقابل سیگنالهای سایر کلاسها می گذاریم و با CSP دو کلاسه الگوهای فضایی مخصوص همین کلاس را محاسبه می کنیم. برای رده بندی سیگنال جدید، آن سیگنال را بر همهٔ فیلترهای فضایی نگاشت می‌کنیم، واریانس سیگنالهای نگاشت شده را حساب می‌کنیم و از LDA چند کلاسه استفاده می‌کنیم.

### راه سوم: قطری سازی همزمان
ترفند اصلی CSP دو کلاسه قطری سازی همزمان دو ماتریس کواریانس است طوری که حاصلجمع مقادیر ویژهٔ نظیر آن دو برابر یک شود. پس یک روش تعمیم به چند ماتریس کواریانس برای چند کلاس این است که ماتریس <span class="InlineFormula"><b>W</b></span> و ماتریسهای قطری <span class="InlineFormula"><b>D</b><sub><i>i</i></sub> , <i>i</i>=1,…,<i>N</i></span> را بیابیم که عناصر آنها در بازهٔ <span class="InlineFormula">[0,1]</span> قرار دارد طوری که <span class="InlineFormula"><b>WΣ</b><sub><i>i</i></sub><b>W</b><sup>T</sup>=<b>D</b><sub><i>i</i></sub></span> و <math xmlns="http://www.w3.org/1998/Math/MathML" alt="sum of all D i is the identity matrix"><semantics><mrow><mrow><mrow><munderover><mo stretchy="false">∑</mo><mrow><mi>i</mi><mo stretchy="false">=</mo><mn>1</mn></mrow><mi>N</mi></munderover><msub><mrow><mstyle mathvariant="bold"><mrow><mi>D</mi></mrow></mstyle></mrow><mi>i</mi></msub></mrow><mo stretchy="false">=</mo><mstyle mathvariant="bold"><mrow><mi>I</mi></mrow></mstyle></mrow></mrow><annotation encoding="StarMath 5.0">sum from i=1 to N {bold D}_i = bold I</annotation></semantics></math>. برای <span class="InlineFormula"><i>N</i>&gt;2</span> چنین تجزیه ای را تنها می توان به شکل تقریبی انجام داد. الگوریتمهای مختلفی برای قطری سازی تقریبی وجود دارد و ما سه تا از آنها را در منابع [۷،۸،۹] نام برده ایم.

## وابستگی CSP به پیش پردازش
طی آزمایشهای ما با CSP مشخص شد عملکرد این الگوریم به پردازش سیگنال اولیه وابسته است که البته جای تعجب نیست. مهمترین مراحل اولیهٔ پردازش سیگنالی که می توانیم بر نوار مغزی اعمال کنیم کاهش نرخ نمونه برداری، قاب کردن سیگنال و گذراندن سیگنال از صافی بسامد گزین هستند.

گاهی اوقات نرخ نمونه برداری سیگنال در زمان ضبط بیش از حد نیاز کاربردی است. طبق قضیه‌ٔ نایکویست<sup>۴</sup>، نرخ نمونه برداری باید حداقل دوبرابر بیشترین بسامد مولفه های موجود در سیگنال باشد تا بتوانیم سیگنال را بدون ابهام از روی نمونه ها بازسازی کنیم. یکی از مراحل دیگر پیش پردازش سیگنال اعمال یک صافی بسامد گزین است. این صافی ها مولفه هایی با بسامد خارج از یک بازهٔ خاص را تضعیف می کنند. درنتیجه مهم نیست اگر این مولفه ها در سیگنال اولیه وجود داشته باشند یا حذف شوند. کاهش نرخ نمونه برداری با گذراندن سیگنال از یک فیلتر پایین گذر همراه است که مولفه های فرکانسی پر بسامد را حذف می‌کند. به علاوه کاهش نرخ نمونه برداری موجب کاهش قابل توجه حجم داده می شود.

وقتی یک قسمت از یک سیگنال را جدا می کنیم، درواقع داریم از قاب مستطیلی استفاده می‌کنیم. تبدیل فوریهٔ گسستهٔ سیگنال قاب شده مساوی کانولوشن تبدیلهای فوریهٔ سیگنال اولیه و قاب مستطیلی است. تبدیل فوریهٔ قاب مستطیلی نیز فرمی از تابع sinc است. حال اگر سیگنال نوار مغزی شامل مولفه های فرکانسی باشد طوری که بازهٔ زمانی نمونه برداری تعداد صحیحی از تناوب این مولفه‌ها نباشد، قله ها و صفرهای تابع sinc بر فرکانسهای این مولفه ها منطبق نمی‌شود و نتایج قله های تابع sinc را نشان نمی دهند بلکه مولفه هایی متناهی در همهٔ بسامدها نشان می دهند. این امر منشاء نشط طیفی<sup>۵</sup> است. در این حالت خطوط تبدیل فوریه‌ٔ گسسته دقیقأ دامنهٔ مولفه های فرکانسی سیگنال را انعکاس نمی دهند، و خطوط اضافی در کنار خط بسامد هر مولفه آشکار می شود. نشط طیفی را می توان با استفاده از قاب هایی که داده را با شیب ملایم به صفر می رسانند کاهش داد. قابهای متداول در پردازش سیگنال عبارتند از قاب هامینگ<sup>۶</sup>، قاب هان<sup>۷</sup> و قاب مثلثی یا بارتلت<sup>۸</sup>. طی آزمایشهای ما با استفاده از CSP روی دادگان BBCI-II-1-a مشخص شد قاب هامینگ بهترین دقت رده بندی را ایجاد می‌کند. به علاوه ما متوجه شدیم محل بازهٔ قاب در طول محور زمان نیز بر دقت رده بندی تاثیر می گذارد. از آن بیشتر، تاثیر محل قاب بر دقت CSP یکنواخت نیست که نشان می دهد اطلاعات باارزش تفکیک کنندگی طی زمان پس از رویداد تحریک در سیگنال نوار مغزی به شکل یکنواخت توزیع نشده اند.

همانطوی که گفتیم هدف اصلی CSP بهره گیری از اثرات همگامی و یا ناهمگامی مرتبط با رویداد است. پس استفاده از صافی بسامد گزین برای حذف مولفه های بسامدی نامربوط الزامی است. ما به طور کلی بر اساس نتایج پژوهشهای پیشین و آزمایشهای خود برای رمزگشایی ارادهٔ حرکتی از صافی بسامد گزین در بازهٔ ۶ تا ۳۴ هرتز استفاده می کنیم. همچنین ما متوجه شدیم روش طراحی فیلتر نیز بر عملکرد CSP اثر دارد. بهترین نتایج ما با استفاده از طراحی WSI<sup>۹</sup> به دست آمد.

## مزایای الگوریتم پایهٔ CSP
* سادگی درک و پیاده سازی
* استفادهٔ بهینه از همگامی و ناهمگامی مرتبط با رویداد
* قابلیت تعمیم خوب برای مسائل غیر خطی

## نواقص CSP پایه: 
* نادیده گرفتن اطلاعات متمایز کننده در باندهای بسامد مختلف
* حساسیت به نویز، آرتیفکت یا الگوهای خارج از مدل (outliers) : در نمونهٔ دادهٔ دو کلاسهٔ دو بعدی، با تغییر یک نقطهٔ اولیه به خارج از مدل، دقت تشخیص یک کلاس کمتر شده و به همان میزان دقت تشخیص کلاس دیگر افزایش می یابد. این موضوع در مراجع متعددی [3,4,5] نیز تایید شده است.
* تعداد زیاد پارامترها و نیاز به حجم زیاد دادهٔ آموزشی
* این الگوریتم در اصل برای بهره برداری از اثرات ERD و ERS طراحی شده و تضمینی نیست که در سایر موارد نتایج خوبی بدهد. مثلا از شکل موجها یا نحوه‌ٔ تلفیق آنها استفاده کند.

## امکان بهبود CSP
الگوریتم CSP جای زیادی برای بهبود دارد. از CSP معمولا برای تشخیص و رده بندی ارادهٔ حرکتی استفاده می شود. یکی از سرنخهای مهم رده بندی ارادهٔ حرکتی امواج μ است. این امواج با بسامد ۱۰ تا ۷ هرتز در ناحیهٔ فرونتال مرکزی دیده می شوند. این امواج با بازشدن چشم بیمار یا خواب آلودگی تغییر چندانی نمی‌کنند ولی با لمس دست یا صورت فرد، هرگونه حرکت یا حتی تفکر درمورد حرکت این اعضاء موجب تخفیف یا حتی محو امواج μ هردوطرف می شود. ریتم μ را می‌توان ریتم استراحت قشر حرکتی دانست همانگونه که آلفا (α) را می توان ریتم استراحت قشر بینایی دانست. در نتیجه مطلوب است که برای تشخیص ارادهٔ حرکتی پیش از استفاده از CSP ابتدا امواج را از فیلتر میان گذری بگذرانیم تا امواج μ را در مقابل سایر امواج برجسته کند.

مشخص شده که میزان اطلاعات متمایز کننده در باندهای فرکانسی مختلف برای افراد مختلف بسیار متفاوت است. در [بخش دوم این مقاله](../cssp_and_csssp/index.html) الگوریتم های CSSP و CSSSP را معرفی می کنیم. هدف این الگوریتمها بهینه سازی یک صافی فرکانس گزین-فضایی برای هر آزمودنی است.


## نتایج استفاده از CSP روی برخی از مسائل رقابتهای واسط مغز و رایانهٔ برلین
ما از الگوریتم CSP برای حل برخی از مجموعه های دادگان واسط مغز و رایانهٔ برلین استفاده کردیم. مسائل انتخاب شده شامل تشخیص و بازشناسی تصورات حرکتی سوژه های انسانی هستند. بیشتر این مسايل رده بندی شامل دو کلاس هستند، و برای مسائلی که بیش از دو کلاس دارند، ما الگوریتم را تنها روی حالت دو کلاسه آزمایش کرده ایم. 

این نتایج با استفاده از بهترین پیکربندی پارامترهای پیش پردازش به دست آمده اند. برای فهمیدن این پیکربندی در اجراهایی با مقادیر مختلف پارامترها، دقت CSP را روی مجموعه داده های آزمون اندازه گرفتیم. این کار بر خلاف روشی متداول اندازه گیری دقت الگوریتم های رده بندی است، که یادگیری مدل با قسمتی از دادهٔ آموزشی و ارزیابی آن برای تعیین مقدار بهینهٔ پارامترها روی قسمت دیگر انجام می شود و دقت نهایی الگوریتم با ارزیابی روی دادهٔ آزمون به دست می آید. در اینجا ما قصد نداشتیم برتری الگوریتم CSP را نسبت به سایر الگوریتم‌ها نشان دهیم، بلکه می خواستیم بهترین دقت ممکن را با استفاده از این الگوریتم به دست آوریم. جدول (۱) این نتایج را نشان می‌دهد.

<p class="data-table">
جدول ۱: نتایج آزمایش CSP روی برخی از دادگان رقابتهای واسط مغز و رایانه‌ٔ برلین که هدف رده بندی ارادهٔ حرکتی بوده است.
<table border="1" cellpadding="2" style="width: 100%;direction: rtl;text-align: center;">
<thead><th style="text-align: center;">نام دادگان (زیر مسأله)</th><th colspan="5" style="text-align: center;">دقت رده بندی هر آزمودنی (٪)</th><th style="text-align: center;">دقت رده بندی میانگین (٪)</th></thead>
<tr><td>BBCI-II-3</td><td colspan="5">۸۷/۸۶</td><td>۸۷/۸۶</td></tr>
<tr><td>BBCI-III-1</td><td colspan="5">۸۸/۰۰</td><td>۸۸/۰۰</td></tr>
<tr><td>BBCI-III-3a (LR)</td><td colspan="2">۶۲/۶۴</td><td colspan="2">۷۱/۶۷</td><td>۶۸/۳۳</td><td>۶۷/۵۵</td></tr>
<tr><td>BBCI-III-3a (LF)</td><td colspan="2">۶۵/۵۶</td><td colspan="2">۶۵/۰۰</td><td>۷۰/۰۰</td><td>۶۶/۸۵</td></tr>
<tr><td>BBCI-III-3a (LT)</td><td colspan="2">۶۷/۴۲</td><td colspan="2">۷۰/۰۰</td><td>۶۵/۰۰</td><td>۶۷/۴۷</td></tr>
<tr><td>BBCI-III-3a (RF)</td><td colspan="2">۶۰/۴۴</td><td colspan="2">۷۳/۳۳</td><td>۶۸/۳۳</td><td>۶۷/۳۷</td></tr>
<tr><td>BBCI-III-3a (RT)</td><td colspan="2">۶۸/۸۹</td><td colspan="2">۶۸/۳۳</td><td>۶۱/۶۷</td><td>۶۶/۳</td></tr>
<tr><td>BBCI-III-3a (FT)</td><td colspan="2">۶۶/۲۹</td><td colspan="2">۷۱/۶۷</td><td>۶۶/۶۷</td><td>۶۸/۲۱</td></tr>
<tr><td>BBCI-III-3b</td><td colspan="2">۵۸/۴۴</td><td colspan="2">۵۴/۱۷</td><td>۵۳/۱۵</td><td>۵۵/۲۵</td></tr>
<tr><td>BBCI-III-4a</td><td>۷۲/۳۲</td><td>۹۸/۲۱</td><td>۶۸/۳۷</td><td>۸۴/۳۸</td><td>۵۸/۳۳</td><td>۷۶/۳۲</td></tr>
</table>
</p>

 

***
## پانویسها
1. Common Spatial Patterns
2. Trial
3. Overfit
4. Niquist
5. Spectral Leakage
6. Hamming
7. Hann
8. Bartlett
9. Windowed Shifted Ideal filter

## مراجع

1. Dornhege, Guido, (2006), Optimizing spatio-temporal filters for improving Brain-Computer Interfacing, Advances in Neural Inf. Proc. Systems (NIPS 05), Vol. 18, Cambridge, MA, 2006, MIT Press
2. Blankertz, Benjamin, (2008), Optimizing Spatial Filters for Robust EEG Single-Trial Analysis, IEEE Signal Processing Magazine, VOL. XX, 2008
3. Samek, Wojciech, (2013), Robust Spatial Filtering with Beta Divergence, ??
4. Farquhar, J., (2006), Regularized CSP for sensor selection in BCI, Max Planck Institute for Biological Cybernetics, Tübingen, Germany
5. Fabien Lotte, Cuntai Guan. Regularizing Common Spatial Patterns to Improve BCI Designs: Unified
Theory and New Algorithms. IEEE Transactions on Biomedical Engineering, Institute of Electrical and Electronics Engineers, 2011, 58 (2), pp.355-362. &lt;inria-00476820v4&gt;
6. Dornhege, Guido, (2004), Boosting bit rates in non-invasive EEG single-trial classifications by feature combination and multi-class paradigms. IEEE Trans. Biomed. Eng., 51(6):993–1002, June 2004.
7. Cardoso, J. F., (1996), Jacobi angles for simultaneous diagonalization, SIAM J.Mat.Anal.Appl., 17(1): 161 ff., 1996.
8. Pham, D. T., (2001), Joint Approximate Diagonalization of Positive Definite Matrices, SIAM J. on Matrix Anal. and Appl., 22(4): 1136–1152, 2001
9. Ziehe, A., (2003), A Linear Least-Squares Algorithm for Joint Diagonalization, Proc. 4th International Symposium on Independent Component Analysis and Blind Signal Separation (ICA2003), 469–474, Nara, Japan, 2003

۱۰. واحدی، محمد ابراهیم، (۱۳۶۸)، الکتروانسفالوگرافی، انتشارات اطلاعات



