---
title: "دربارهٔ جاوااسکریپت ها"
description: "این صفحه جاوا اسکریپتهای موجود در وبلاگ و مجوز هرکدام را نشان می دهد"
tags: []
categories: []
date: 2019-02-05T22:30:00+03:30
draft: false
---
این صفحه جدولی از جاوا اسکریپتهای موجود در صفحات این وبلاگ و مجوز هرکدام را نشان می دهد. این جدول با توجه به [جنبش جاوا اسکریپت آزاد](https://www.fsf.org/campaigns/freejs) و برای انطباق این وبلاگ با سیاستهای آن جنبش افزوده شده است.

<table id="jslicense-labels1" style="width: 100%;direction: ltr;text-align: center;" border="1">
	<tr>
		<td><a href="../../js/shamsi-date.min.js">shamsi-date.min.js</a></td>
   		<td><a href="http://www.gnu.org/licenses/gpl-3.0.html">GPL-3.0</a></td>
        <td><a href="../../js/shamsi-date.js">shamsi-date.js</a></td>
    </tr>
    <tr>
    	<td><a href="@issoPrefix@/js/config.min.js">config.min.js</a></td>
    	<td><a href="https://mit-license.org/">MIT</a></td>
    	<td><a href="@issoPrefix@/js/config.js">config.js</a></td>
    </tr>
    <tr>
    	<td><a href="@issoPrefix@/js/components/requirejs/require.js">require.js</a></td>
    	<td><a href="https://mit-license.org/">MIT</a></td>
    	<td><a href="@issoPrefix@/js/components/requirejs/require.js">require.js</a></td>
    </tr>
</table>
