
	
help :
	@echo -e "\nUse one of the targets below:\n"
	@echo -e "'make publish' will generate a minified version of weblog, adds neccessary external content and publishes it onto production server. This weblog will contain final articles only\n"
	@echo -e "'make localserver' will start a local server\n"
	@echo -e "'make sync-config' will update configurations on the production host"

publish : config.toml
	@hugo-ex --minify --destination public
	./inline-script-license.sh
	@tempfilename=`mktemp`; \
	sed -e "s%@issoPrefix@%`./config.sh --isso-prefix`%g" ./public/about/javascripts/index.html > $$tempfilename; \
	cp $$tempfilename ./public/about/javascripts/index.html
	@echo "Starting to upload onto `./config.sh --remote-ip`..."
	@rsync -rz --progress --delete -e "ssh -p `./config.sh --ssh-port`" public/* babak@`./config.sh --remote-ip`:/var/www/html/cs/

config.toml : config.toml.in config.sh
	@sed -e "s%@baseurl@%`./config.sh --final-domain`%; s%@issoPrefix@%`./config.sh --isso-prefix`%; s%@rootPath@%`./config.sh --root-path`%" config.toml.in > config.toml

localserver : localserverconfig.toml
	@hugo-ex server -Dv --debug --config localserverconfig.toml


localserverconfig.toml : config.toml.in
	@sed -e "s%@baseurl@%http://localhost:1313%; s%@issoPrefix@%http://localhost:8181%; s%@rootPath@%%" config.toml.in > localserverconfig.toml
	
sync-config : private/isso.conf config.sh
	@scp $< babak@`./config.sh --remote-ip`:/var/isso/csweblog/config


.PHONY: help localserver publish sync-config
