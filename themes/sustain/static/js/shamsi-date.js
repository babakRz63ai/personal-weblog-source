/**************************************************************************
	Converts Gregorain dates to Jalaali dates used in Iran.
	
	Copyright (C) 2019 Babak Razmjoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

	To contact with the author mail to : b.razmjoo@protonmail.com
***************************************************************************/

function convertDateStringToShamsi(s)
{
	regex=new RegExp("(\\d{4})-(\\d{1,2})-(\\d{1,2}).*","");
	m=regex.exec(s);
	if(m) 
		return convertDateToShamsi(new Date(m[1],m[2]-1,m[3],12,0,0));
	else
	{ 
		console.error("\""+s+"\" is not matched with the date pattern");
		return null;
	}
}
function transFarsiDigits(s)
{
	res="";
	farsiDigit=new Array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹');
	for (i=0;i<s.length;i++)
	{
		code = s.charCodeAt(i);
		if (48<=code && code<=57)
			res += farsiDigit[code-48];
		else
			res += s.charAt(i);
	}
	return res;
}
function convertDateToShamsi(date)
{
week=new Array("یكشنبه","دوشنبه","سه شنبه","چهارشنبه","پنج شنبه","جمعه","شنبه");
months=new Array("فروردین","اردیبهشت","خرداد","تیر","مرداد","شهریور","مهر","آبان","آذر","دی","بهمن","اسفند");
day=date.getDate();
month=date.getMonth()+1;
year=date.getFullYear();
if (year==0)
	year=2000;
if (year<100)
	year+=1900;

switch (year%4)
{
	case 0:
		nowrooz = 20;
		dayThreshold = new Array(21,20,20,20,21,21,22,22,22,22,21,21);
		daysToAdd =    new Array(10,11,10,12,11,11,10,10,10, 9,10,10);
		daysToSub =    new Array(21,19,19,19,20,20,21,21,21,21,20,20); 
		break;
	
	case 1:
		nowrooz = 21;
		dayThreshold = new Array(21,19,21,21,22,22,23,23,23,23,22,22);
		daysToAdd =    new Array(11,12,10,11,10,10, 9, 9, 9, 8, 9, 9);
		daysToSub =    new Array(19,18,20,20,21,21,22,22,22,22,21,21);
		break;
	
	default:
		nowrooz = 21;
		dayThreshold = new Array(21,20,21,21,22,22,23,23,23,23,22,22);
		daysToAdd = new Array(10, 11, 9, 11, 10, 10, 9, 9, 9, 8, 9, 9);
		daysToSub = new Array(20, 19, 20,20, 21, 21,22,22,22,22,21,21); 
}

shamsiMonth = month-3;
if (shamsiMonth<1)
	shamsiMonth += 12;
year-=((month<3)||((month==3)&&(day<nowrooz)))?622:621;
	if (day<dayThreshold[month-1])
		day += daysToAdd[month-1];
	else
	{
		day -= daysToSub[month-1];
		shamsiMonth++;
	}
	
return transFarsiDigits(week[date.getDay()]+" "+day+" "+months[shamsiMonth-1]+" "+year);
}
