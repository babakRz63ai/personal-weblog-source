#!/usr/bin/sh

LICENSE="\n/*\n\
        @licstart  The following is the entire license notice for the\n\
        JavaScript code in this page.\n\
\n\
        Copyright (C) 2019  Babak Razmjoo\n\
\n\
        The JavaScript code in this page is free software: you can\n\
        redistribute it and/or modify it under the terms of the GNU\n\
        General Public License (GNU GPL) as published by the Free Software\n\
        Foundation, either version 3 of the License, or (at your option)\n\
        any later version.  The code is distributed WITHOUT ANY WARRANTY;\n\
        without even the implied warranty of MERCHANTABILITY or FITNESS\n\
        FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.\n\
\n\
        As additional permission under GNU GPL version 3 section 7, you\n\
        may distribute non-source (e.g., minimized or compacted) forms of\n\
        that code without the copy of the GNU GPL normally required by\n\
        section 4, provided you include this license notice and a URL\n\
        through which recipients can access the Corresponding Source.\n\
\n\
        @licend  The above is the entire license notice\n\
        for the JavaScript code in this page.\n\
        */\n"
        

# Recursive function to insert the license above between the first '<script></script>' pair
insert_script_license()
{
	for name in `ls $1/*.html`; do
		echo "Inserting license into $name..."
		temp=`mktemp`
		sed -e "s%<body><script></script>%<body><script>$LICENSE</script>%" $name > $temp;
		cp $temp $name
	done
	
	for name in `ls $1`; do
		if test -d $1/$name; then
			insert_script_license $1/$name;
		fi
	done		
} 

if test ! -d public; then
	echo "There must be 'public' directory"
	exit 1;
fi

insert_script_license public


